<?php
/**
* Template Name: Home Page
*
* @package WordPress
*/
get_header();
if ( have_posts() ) : while ( have_posts() ) : the_post(); 
	$fields = get_fields(); 
	include(locate_template('/template-parts/hero.php'));
?>
<div id="cms">
	<?php ajsCMS('cms'); ?>
</div>
<?php 
endwhile; endif; ?>
<?php get_footer(); ?>
