<a class="banner <?=$class; ?>" href="<?=$bannerFields['content']['link']['url']; ?>" title="<?=get_the_title(); ?>" style="background-color: <?=$bannerFields['content']['background']; ?>" >
					<div class="text">
						<strong class="title"><?=$bannerFields['content']['title']; ?></strong>
						<?=$bannerFields['content']['snippet']; ?>
						<?php if($bannerFields['content']['link']['title']) { ?>
							<span class="button"><?=$bannerFields['content']['link']['title']; ?></span>
						<?php } ?>
						<p class="logo"><?=ajsImage($bannerFields['content']['logo']['id']); ?></p>
					</div>
					<div class="background">
						<?=$background; ?>
					</div>
				</a>
				<style>
					.banner-<?=get_the_ID(); ?> .text {
					background: -moz-linear-gradient(left,  <?=$bannerFields['content']['background']; ?>FF 0%,<?=$bannerFields['content']['background']; ?>AA 66%, <?=$bannerFields['content']['background']; ?>00 100%); /* FF3.6-15 */
					background: -webkit-linear-gradient(left,  <?=$bannerFields['content']['background']; ?>FF 0%,<?=$bannerFields['content']['background']; ?>AA 66%,<?=$bannerFields['content']['background']; ?>00 100%); /* Chrome10-25,Safari5.1-6 */
					background: linear-gradient(to right,  <?=$bannerFields['content']['background']; ?>FF 0%,<?=$bannerFields['content']['background']; ?>AA 66%,<?=$bannerFields['content']['background']; ?>00 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */

						
					}
				</style>