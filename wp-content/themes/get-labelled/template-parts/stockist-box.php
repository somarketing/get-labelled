<?php 
$subfields = get_fields();
?>
<div class="js-location-marker" data-title="<?php the_title(); ?>" data-address="<?=$subfields['address']; ?>" data-latitude="<?=$subfields['location']['latitude']; ?>" data-longitude="<?=$subfields['location']['longitude']; ?>">
	<h3><?php the_title(); ?></h3>
	<?php if($distance) { ?><p class="distance">(<?=$distance; ?> miles away)</p><?php } ?>
	<p>T: <?=$subfields['phone']; ?></p>
	<address>
		<p><?=$subfields['address']; ?></p>
	</address>
</div>
