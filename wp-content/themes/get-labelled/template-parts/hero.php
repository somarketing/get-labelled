<?php
$hero = $fields['hero'];
$title = $hero['title']['line_1'];
$subTitle = $hero['title']['line_2'];
$subTitle = explode(PHP_EOL,$subTitle);
$products = $hero['products'];
?>
<div id="hero">
	<div class="swiper-container">
		<div class="swiper-wrapper">
			<?php foreach ($hero as $slide) {
				$class = '';
				$class .= $slide['acf_fc_layout'].' ';
				// print_r($slide);
			 ?>
			<div class="hero-slide swiper-slide">
				<?php 

				switch ($slide['acf_fc_layout']) {
					case 'text_left':
					case 'text_right':
					case 'text_center':
					
					if($slide['content']['product']['id']) $class .= 'image ';
					?>
					<div class="content <?=$class; ?>">
						<div class="container">
							<div class="text">
								<?php
								$html = '';
								$html .= '<strong class="title">'.$slide['content']['title'].'</strong>';
								$html .= '<p class="snippet">'.$slide['content']['snippet'].'</p>';
								$html .= '<p class="desktop">'.$slide['content']['text'].'</p>';
								if($slide['content']['link']['url']) {
									$html .= '<a class="link" href="'.$slide['content']['link']['url'].'" title="'.$slide['content']['link']['title'].'">'.$slide['content']['link']['title'].'</a>';
								}
								if($slide['content']['product']['id'] !== null) {
									$html .= ajsImage($slide['content']['product']['id'],'full',true);
								}
								echo $html;
								?>
							</div>
						</div>
					</div>
					<?php break;
					case 'grid': ?>
					<div class="content <?=$class; ?>">
						<div class="container count-<?=count($slide['content']['group']); ?>">
							<?php foreach($slide['content']['group'] as $group) {
								$groupClass = '';
								if($group['product']['id']) $groupClass .= 'image ';
							?>
							<div class="text <?=$groupClass; ?>">
									<?php
										// print_r($group);
										$html = '';
										$html .= '<strong class="title">'.$group['title'].'</strong>';
										$html .= '<p class="snippet">'.$group['snippet'].'</p>';
										$html .= '<p class="desktop">'.$group['text'].'</p>';
										if($group['link']['url']) {
											$html .= '<a class="link" href="'.$group['link']['url'].'" title="'.$group['link']['title'].'">'.$group['link']['title'].'</a>';
										}
										if($group['product']['id'] !== null) {
											$html .= ajsImage($group['product']['id'],'full',true);
										}
										echo $html;
									?>
							</div>
							<?php } ?>
						</div>
					</div>
					<?php break;
					default:
						# code...
						break;
				} ?>
				<picture class="background">
					<?php if($slide['images']['desktop']) { ?>
						<source srcset="<?=$slide['images']['desktop']['sizes']['hero-desktop']; ?>" media="(min-width: 1025px)" />
					<?php } ?>
					<?php if($slide['images']['desktop']) { ?>
						<source srcset="<?=$slide['images']['desktop']['sizes']['hero-tablet']; ?>" media="(min-width: 769px) and (max-width: 1024px)" />
					<?php } ?>
					<?php if($slide['images']['mobile']) { ?>
						<source srcset="<?=$slide['images']['mobile']['sizes']['hero-mobile']; ?>" media="(max-width: 768px)" />
						<img src="<?=$slide['images']['mobile']['url']; ?>" alt="<?=$slide['images']['mobile']['alt']; ?>" width="768" height="768" />
					<?php } ?>
				</picture>

				
			</div>
			<?php } ?>
		</div>
	</div>
	<nav>
		<button class="swiper-nav swiper-prev"></button>
		<button class="swiper-nav swiper-next"></button>
	</nav>
</div>