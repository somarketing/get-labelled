<?php 
	$fields = get_fields('cpt_product');

	if(is_page()) {
		$fields = get_fields();
	}

	if( is_home() || is_single() || is_category() || is_tax() ) {
		$id = get_option('page_for_posts', true);
		$fields = get_fields($id);
	}
	if ( class_exists( 'WooCommerce' ) ) {
		if(is_woocommerce()) {
			$id = get_queried_object_id();
			$term = get_term( $id, 'product_cat' );
			$fields = get_fields('cpt_product');

			$desktop 	= $fields['page_title']['desktop']['sizes']['shop-head-desktop'];
			$tablet 	= $fields['page_title']['desktop']['sizes']['shop-head-tablet'];
			$mobile 	= $fields['page_title']['mobile']['sizes']['shop-head-mobile'];
			if(get_fields('product_cat_'.$id)) {

			    $fields = get_fields('product_cat_'.$id);


			}
		}
	}

	$page_title = $fields['page_title'];

	if(is_home() || (is_single())) {
		$page_title['title_1'] = 'Latest';
		$page_title['title_2'] = 'News';
	}

	if(is_category() || is_tax()) {
		if($page_title['title_2'] == '') {
			$page_title['title_2'] = single_term_title('',false);
		}
	}
	if(isset($page_title['desktop']['id'])) {
		$desktop 	= $page_title['desktop']['sizes']['page-head-desktop'];
		$tablet 	= $page_title['desktop']['sizes']['page-head-tablet'];
		$mobile 	= $page_title['mobile']['sizes']['page-head-mobile'];
		if ( class_exists( 'WooCommerce' ) ) {
			if(is_woocommerce() || is_cart() || is_checkout() || is_account_page() || is_home() || is_single()) {
				$desktop 	= $page_title['desktop']['sizes']['shop-head-desktop'];
				$tablet 	= $page_title['desktop']['sizes']['shop-head-tablet'];
				$mobile 	= $page_title['mobile']['sizes']['shop-head-mobile'];
			}
		}
	}



	$class = '';
	if(strlen($page_title['title_2']) > 15) {
		$class .= ' small';
	}

	$short_description = '';
	if($page_title['short_description']) $short_description = '<p>'.$page_title['short_description'].'</p>';
?>
<div id="page-header" class="<?=$class; ?>">
	<div>
		<header>
			<div class="text">
				<h1>
					<span><?=$page_title['title_1']; ?></span>
					<strong><?=$page_title['title_2']; ?></strong>
				</h1>
				<?=$short_description; ?>
			</div>
			<picture>
				<source srcset="<?=$desktop; ?>" media="(min-width: 1025px)" />
				<source srcset="<?=$tablet; ?>" media="(min-width: 769px)" />
				<source srcset="<?=$mobile; ?>" media="(max-width: 768px)" />
				<img src="<?=$mobile; ?>" title="<?=$page_title['title_2']; ?>" />
			</picture>
		</header>
		<?php if ( class_exists( 'WooCommerce' ) ) { woocommerce_breadcrumb(); } else { the_breadcrumb(); }?>
	</div>
</div>