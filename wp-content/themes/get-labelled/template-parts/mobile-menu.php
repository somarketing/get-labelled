<?php global $options; ?>
<div id="mobile-menu">
  <button class="close" aria-label="Close Menu"></button>
  <div>
    <nav>
      <?php wp_nav_menu( array(
        'theme_location' => 'mobile-menu',
        'container' =>false,
        'menu_class' => 'mobile-menu menu',
        'menu' => 'mobile-menu',
        'menu_id' => 'mobile-menu-menu',
        'echo' => true,
        'before' => '',
        'after' => '',
        'link_before' => '',
        'link_after' => '',
        'depth' => 1)
      ); ?>
      <?=ajsSocial(); ?>
    </nav>
    <div class="contact-details">
      <address>
        <strong class="title">Contact</strong>
          <address>
            <?=$options['address']; ?>
          </address>
          <p><a target="_blank" href="tel:<?=phoneURL($options['phone']); ?>" title="Call <?=get_bloginfo('name');?> on <?=$options['phone']; ?>"><?=$options['phone']; ?></a></p>
          <p><a target="_blank" href="mailto:<?=$options['email']; ?>" title="Email <?=get_bloginfo('name');?> at <?=$options['email']; ?>"><?=$options['email']; ?></a></p>
      </address>
      <p>©<?=get_bloginfo('name');?> <?=date("Y"); ?>. All rights reserved</p>
    </div>
  </div>
</div>