<?php get_header(); ?>
<?php 
	$fields = get_fields();
	$current_url = 'https://' . $_SERVER['SERVER_NAME'] .$_SERVER['REQUEST_URI'];
 	?>
	<div id="blog">
		<?php  include(locate_template('/template-parts/page-title.php')); ?>
		<section id="blog-grid">
			<nav class="category-nav">
				<?php 
				$url = get_permalink(get_option( 'page_for_posts' ));
	          	if (strpos($current_url,$url) !== false) {
				    $class = "active";
				} else {
				    $class = "";
				}
				?>
		      	<a class="<?=$class; ?>" href="<?=$url; ?>"  title="All Posts">All Posts</a>
		      	<?php
			        $args = array(
			          'parent' => 0
			        );
			        $terms = get_terms( 'category',$args);
			        if ($terms) {
			          foreach ($terms as $term) {
			          	$url = esc_url( get_term_link( $term ) );
			          	if (strpos($current_url,$url) !== false) {
						    $class = "active";
						} else {
						    $class = "";
						}
			            $term_id = $term->term_id;
			            $taxonomy_name = 'category';
			            $terms = get_term_children( $term_id, $taxonomy_name );
			             echo '<a class="'.$class.'" href="'.$url.'" title="'.$term->name.'">'.$term->name.'</a>';
			          }
			        }
		    	?>
		    </nav>
		    <div class="post-grid">
				<?php 
				if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<a class="post-box" href="<?=get_the_permalink(); ?>" title="<?=get_the_title(); ?>">
						<article >
							<h3><?php the_title(); ?></h3>
							<div class="img">
								<?=ajsImage('','photo'); ?>	
							</div>
							<p class="excerpt">
								<?=get_the_excerpt(); ?>
							</p>
							<span class="read-more">Read More</span>
						</article>
					</a>
				<?php endwhile; endif; ?>
			</div>
			<?php pagination(); ?>
		</section>
	</div>
<?php get_footer(); ?>
