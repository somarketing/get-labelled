<?php get_header(); ?>
<?php 
if ( have_posts() ) : while ( have_posts() ) : the_post();
	$fields = get_fields();
 	?>
 	<?php get_template_part('/templates/partials/banner'); ?>
	<div id="page">
		<?php  include(locate_template('/template-parts/page-title.php')); ?>
		<div id="cms">
			<?php 
			if(get_field('cms')) {
				ajsCMS('cms');
			} else { ?>
				<div id="single-page">
					<section class="wysiwyg">
						<?php the_content(); ?>
					</section>
				</div>
			<?php } ?>
		</div>
	</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>