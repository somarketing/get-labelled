<?php
/**
* Template Name: Woo Page
*
* @package WordPress
*/
get_header(); ?>
<?php 
if ( have_posts() ) : while ( have_posts() ) : the_post();
	$fields = get_fields();
 	?>
	<div id="page">
		<?php  include(locate_template('/template-parts/page-title.php')); ?>
		<div id="woo-page">
			<section>
				<?php the_content(); ?>
			</section>
		</div>
	</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
