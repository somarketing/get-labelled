function carousel_hero() {
  var swiper = new Swiper('#hero .swiper-container', {
    slidesPerView: 1,
    spaceBetween: 0,
    autoplay: {
      delay: 5000,
      disableOnInteraction: true,
    },
    navigation: {
      onlyInViewport: 'true',
      nextEl: '#hero .swiper-next',
      prevEl: '#hero .swiper-prev',
    }
  });
};

carousel_hero();

function carousel_products() {
  var swiper = new Swiper('.featured-products .swiper-container', {
    slidesPerView: 1,
    spaceBetween: 30,
    autoplay: {
      delay: 5000,
      disableOnInteraction: true,
    },
    navigation: {
      onlyInViewport: 'true',
      nextEl: '.featured-products .swiper-next',
      prevEl: '.featured-products .swiper-prev',
    },
    // init: false,
    breakpoints: {
      380: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 3,
      },
      1025: {
        slidesPerView: 4,
        spaceBetween: 30,
      },
      1600: {
        slidesPerView: 5,
        spaceBetween: 50,
      }
    }
  });
};

carousel_products();

function carousel_product() {
  var swiper = new Swiper('#product-gallery .swiper-container', {
    slidesPerView: 1,
    spaceBetween: 30,
    navigation: {
      onlyInViewport: 'true',
      nextEl: '.product-next',
      prevEl: '.product-prev',
    },
  });
};

carousel_product();

function carousel_brands() {
  var swiper = new Swiper('.brands .swiper-container', {
    slidesPerView: 3,
    loop: true,
    spaceBetween: 30,
    autoplay: {
      delay: 5000,
      disableOnInteraction: true,
    },
    // init: false,
    breakpoints: {
      380: {
        slidesPerView: 3,
      },
      768: {
        slidesPerView: 5,
      },
      1025: {
        slidesPerView: 7,
        spaceBetween: 30,
      },
      1600: {
        slidesPerView: 7,
        spaceBetween: 50,
      }
    }
  });
};

carousel_brands();


(function($) {


  function qty() {
    $(document).on('click touch','.quantity button', function(e) {
        e.preventDefault();
        $('.shop_table button').removeAttr('disabled');
        var wrap,qty,btn,step;
        btn = $(this);
        wrap = $(this).closest('.quantity');
        qty = wrap.find('input[type=text]');
        step = parseInt(qty.attr('step'));
        if(btn.hasClass('fa-plus')) {
            var x, max;
            max = parseInt(qty.attr('max'));
            x = parseInt(qty.val());
            x = parseInt(x + step);
            if(max !== '') {
              if((x !== 'undefined') && (x > max)) {
                x = max;
              }
            }
            qty.val(x);
        }
        if(btn.hasClass('fa-minus')) {
            var x, min;
            min = parseInt(qty.attr('min'));
            x = parseInt(qty.val());
            x = parseInt(x - step);
            if((x !== 'undefined') && (x < min)) {
              x = min;
            }
            qty.val(x);
        }
    });
  }
  qty();


  function featuredVideo() {
    $(document).on('click touch', '.video-cms button',function() {
      var wrapper, btn, video;
      btn = $(this);
      wrapper = $(this).parent();

      video = wrapper.find('video');
      wrapper.addClass('active');
      video.get(0).play();
      video.attr('controls','true');
    });
  }

  featuredVideo();

  function mobileMenu() {
    $(document).on('click touch','#toggle-navigation',function(e) {
      e.preventDefault();

      $('html').toggleClass('mobile-menu-open'); 
    });

    $(document).on('click touch','.close',function(e) {
      $('html').removeClass('mobile-menu-open');
    }); 
  }
  mobileMenu();

  function sidebarToggle() {
    $(document).on('click touch','#sidebar-toggle',function(e) {
      e.preventDefault();

      $('html').toggleClass('sidebar-open');
      if($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).html('<span class="fa fa-ballot-check"></span> <strong>Filter Products</strong>');
      } else {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#product-archive").offset().top
        }, 1000);
        $(this).addClass('active');
        $(this).html('<span class="close"></span>');
      }
    });
  }

  sidebarToggle();

  function accordion() {
    $(document).on('click touch','.accordion > button',function() {
      var button, target, accordion, siblings;
      accordion = $(this).closest('.accordion');
      button = $(this);
      target = '#'+button.data('target');
      
      target = $(target);
      
      //close other tabs
      accordion.height(accordion.height()); //set height of accordion to stop page jumping

      if(button.hasClass('open')) {
        if($(window).width() < 1025) {
          button.removeClass('open');
          target.removeClass('open');
          target.hide();
        }
      } else {
        accordion.find('section, button').removeClass('open');
        siblings = accordion.find('section');
        siblings.hide();

        button.addClass('open');
        target.addClass('open');
        
        target.fadeIn(300);
        setTimeout(function() {
          accordion.height(''); //unset height of accordion to reset height to childs new height
        },300);
      }
    });
  }

  accordion();

  if($(window).width() > 1600) {
    $(document).on('scroll',function() {
       var scrollPoz = $(document).scrollTop();
       
       scrollPoz = 5.7-(scrollPoz/50);
       // console.log(scrollPoz);
       $('#product-gallery>div .image img').css('transform','translateY('+scrollPoz+'%)');
    });
  }

  
 function enquiryForm() {
  $(document).on('click touch','form.so_form input[type=submit], form.so_form .submit', function(e) {
      e.preventDefault();

      var form, data, fields, error, debug, send;
      
      form = $(this).closest('form');
      fields = form.find('input');
      error = 'Please fill out the required fields carefully and in full.';
      debug = false;
      send = form.serialize();

      if(debug == true) {
        console.log(send);
        console.log('form submitted');
      }
      data = {
        'action' : 'enquiry_form_submit',
        'form_data' : send
      };
      $('.invalid').removeClass('invalid');
      $('.error').remove();

      function mark_error(form,field,error, debug) {
        form.addClass('invalid');
        form.find('.alert').html('<span>'+error+'</span>');
        field.closest('label').addClass('invalid');
        // field.closest('label').append('<strong class="error error-required">Required</strong>');
        if(debug == true) {
          field.css('border','1px solid #f00');
        }
      }

      fields.each(function() {
        var field;
        field = $(this);
        if(field.attr('type') !== 'hidden') {
          if(field.is(':valid')) {
          } else {
            mark_error(form,field,error, debug);
          }
        }
      });
      if(!form.find('.invalid').length) { 
        if(debug == true) { 
          console.log('valid submission');
        }
        $.post(adminurl, data, function(data) {
          if(data != '') {
            form.html(data);
            form.hide().fadeIn(300);
          } else {
            if(debug == true) {
              $('html').html('woops bad data');
            }
          }
        });
      } else {
        if(debug == true) {
          form.prepend('<pre>Form Submitted'+data+'</pre>');
          console.log('invalid submission');
        }
      }
    });
  }
  enquiryForm();

  function so_footer() {
    if(!$('.so-marketing').length) {
      // alert('Loading the stuff');
      var data = {
          'action' : 'so_footer'
      };
      $.post(
        adminurl, data, function(data) {
          if(data != '') {
            $('.so-stuff').html(data);
            $('.so-marketing').hide();
            $('.so-marketing').fadeIn(300);
          }
        }).done(function() {
      });
    }
  };
  so_footer();

  function product_count() {
    // alert('Loading the stuff');
    var data = {
        'action' : 'product_count'
    };
    $.post(
        adminurl, data, function(data) {
        if(data != '') {
          // alert(data);
          data = $.parseJSON(data);
          $('#toggle-basket .count').remove();
          $('#toggle-basket').append(data['cart_count']);
          $('#toggle-basket .count').hide();
          $('#toggle-basket .count').fadeIn(300);
        } else {
          $('#toggle-basket .count').remove();
        }
    }).done(function() {
    });
  };
  setTimeout(function() {
    product_count();
  },1000);


  var noobie = true;
  if(sessionStorage.getItem("noobie")) {
    noobie = sessionStorage.getItem("noobie");
  }
  if(noobie == true) {
    setTimeout(function() {
      var data = {
        'action' : 'newsletter_form_pop_submit'
      };
      $.post(
          adminurl, data, function(data) {
          // alert('Attempting to load the stuff');
          if(data != '') {
            $('html').append(data);
          } else {
            // $('html').html('woops');
          }
      }).done(function() {
          // alert('mobile menu loaded');
      });
    },4000);
  }

  $(document).on('submit','#newsletter-signup form',function(e) {
      e.preventDefault();
      setTimeout(function() {
        var email = $('#newsletter-signup .js_email_placeholder').val();
        var data = {
          'action' : 'newsletter_form_submit'
        };
        $.post(
            adminurl, data, function(data) {
            // alert('Attempting to load the stuff');
            if(data != '') {
              $('#wrapper').append(data);
              console.log(email);
              $('#enquiry-form-confirmation input[type=email]').val(email);

            } else {
              // $('html').html('woops');
            }
        }).done(function() {
            // alert('mobile menu loaded');
        });
      },300);
  });


  $(document).on('click touch','#enquiry-form-confirmation .submit',function() {
    sessionStorage.setItem("noobie", false);
    setTimeout(function() {
      if(!$('#mce-success-response').is(':empty')) {
        $('#enquiry-form-confirmation').remove();
      }
    },3000);
  });

  $(document).on('click touch','#enquiry-form-confirmation .closer',function() {
    sessionStorage.setItem("noobie", false);
    $('#enquiry-form-confirmation').fadeOut(1000);
    setTimeout(function() {
      $('#enquiry-form-confirmation').remove();
    },1000);
  });


 $(function() {
      $("input[type=tel]").on('input', function(e) {
          $(this).val($(this).val().replace(/[^0-9]/g, ''));
      });
  });

})( jQuery );

