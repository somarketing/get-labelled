<?php
/**
* Template Name: Contact Page
*
* @package WordPress
*/
get_header(); ?>
<?php 
if ( have_posts() ) : while ( have_posts() ) : the_post();
	$fields = get_fields();

include(locate_template('/template-parts/page-title.php')); ?>
<div id="contact-us">
	<div>
		<section class="wysiwyg">
			<?php the_content(); ?>
			<address>
				<?=$options['address']; ?>
			</address>
			<p>Tel:  <a target="_blank" href="tel:<?=phoneURL($options['phone']); ?>" title="Call us on <?=$options['phone']; ?>"><?=$options['phone']; ?></a></p>
			<p>Email: <a target="_blank" href="mailto:<?=$options['email']; ?>" title="Contact us at <?=$options['email']; ?>"><?=$options['email']; ?></a></p>
		</section>
		<?=so_enquiryform();?>
	</div>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>