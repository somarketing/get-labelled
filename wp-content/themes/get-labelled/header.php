<?php // header("Location: /wp-admin/"); ?>
<?php global $options; ?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link rel="manifest" href="site.webmanifest"> -->
  <?php wp_head(); ?>
  <script>document.documentElement.className='fadeDoc';</script>
  <meta name="theme-color" content="#2C2C54">
  <link rel="preconnect" href="https://use.typekit.net">
  <?=$options['facebook_api']; ?>
</head>
<body <?php body_class(); ?>>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->
  
    <header id="header">
      <div>
        <?=get_the_logo(); ?>
        <?php if ( class_exists( 'WooCommerce' ) ) { ?>
          <a href="/basket/" id="toggle-basket" title="Open Shipping Basket"></a>
        <?php } ?>
        <button id="toggle-navigation" aria-label="Toggle Mobile Navigation"></button>
        <nav id="main-menu">
          <?php wp_nav_menu( array(
           'container' =>false,
           'menu_class' => 'main-menu',
           'menu' => 'main-menu',
           'echo' => true,
           'before' => '',
           'after' => '',
           'link_before' => '',
           'link_after' => '',
           'depth' => 2,
           'walker' => new header_walker())
           ); ?>
        </nav>
      </div>
    </header>
    <main id="page_content">