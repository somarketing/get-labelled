<?php get_header(); ?>
<?php 
	$fields = get_fields('cpt_product');
	$page_title = $fields['page_title'];

	$desktop 	= $page_title['desktop']['sizes']['page-head-desktop'];
	$tablet 	= $page_title['desktop']['sizes']['page-head-tablet'];
	$mobile 	= $page_title['mobile']['sizes']['page-head-mobile'];
	if(is_woocommerce() || is_cart() || is_checkout() || is_account_page()) {
		$desktop 	= $page_title['desktop']['sizes']['shop-head-desktop'];
		$tablet 	= $page_title['desktop']['sizes']['shop-head-tablet'];
		$mobile 	= $page_title['mobile']['sizes']['shop-head-mobile'];
	}
 	?>
	<div id="page">
		<div id="page-header">
			<div>
				<header>
					<div class="text">
						<h1>
							<strong>404</strong>
							<span>Page not found</span>
							
						</h1>
					</div>
					<picture>
						<source srcset="<?=$desktop; ?>" media="(min-width: 1025px)" />
						<source srcset="<?=$tablet; ?>" media="(min-width: 769px)" />
						<source srcset="<?=$mobile; ?>" media="(max-width: 768px)" />
						<img src="<?=$mobile; ?>" title="<?=$page_title['title_2']; ?>" />
					</picture>
				</header>
				<?php woocommerce_breadcrumb(); ?>
			</div>
		</div>
		<div id="woo-page">
			<section>
					<h3>404: Sorry the page you are looking<br /> for could not be found</h3>
					<p>The URL may have been moved or deleted. Please check the URL for errors, or alternativly return to our <a href="/" title="<?=get_bloginfo('name');?>">home page</a>.</p>
					<p>If you still cannot find the page you are looking for please <a href="/contact-us/" title="Contact Us">contact our customer service team</a>.</p>
			</section>
		</div>

	</div>
<?php get_footer(); ?>
