<?php
add_action( 'wp_ajax_so_footer', 'so_footer' );
add_action( 'wp_ajax_nopriv_so_footer', 'so_footer' );
  
function so_footer() { ?>
	<style>
		.so-marketing {
			display: flex;
			position: relative;
			padding-right: 3rem;
			padding-left: 0rem;
			height: 2.5rem;
			align-items: center;
			text-transform: uppercase;
			transition: 300ms;
			color: white;
			background-color: inherit;
			width: 12rem;
			color: #a2a1a1;
			font-size: 0.9rem;
		}
		.so-marketing .to-slide {
			position: absolute;
			top: 50%;
			transform: translateY(-50%);
			right: 0;
			left: calc(100% - 2.5rem);
			width: 2.5rem;
			transition: 300ms;
			z-index: 5;
		}
		.so-marketing .to-slide svg {
			width: 2.5rem;
		}
		.so-marketing .to-hide {
			transition: 0ms;
			z-index: 1;
			position: absolute;
			top: 50%;
			transform: translateY(-50%);
			left: 0;

		}
		.so-marketing .to-reveal {
			position: absolute;
			top: 50%;
			left: 4rem;
			right: 0;
			transform: translateY(-50%);
			opacity: 0;
			width: calc(100% - 4rem);
			z-index: 3;
		}
		.so-marketing:after {
			position: absolute;
			top: 0;
			right: 0;
			width: 2.5rem;
			background-color:inherit;
			content: '';
			display: block;
			z-index: 2;
			height: 100%;
		}
		.so-marketing:hover {
			padding-left: 4rem;
			padding-right: 0;
		}

		.so-marketing:hover:after {
			width: 100%;
			transition: 300ms;
		}

		.so-marketing:hover .to-hide {
			opacity: 0;
			color: white;
		} 
		.so-marketing:hover .to-slide {
			left: 0;
			right: calc(100% - 2.5rem);
		}
		.so-marketing:hover .to-reveal {
			opacity: 1;
		}
	</style>
	<a href="http://www.somarketing.com/" target="_blank" rel="noreferrer" title="Website Created By SO Marketing" class="so-marketing">
            <span class="to-hide">Website Created By</span>
            <span class="to-slide"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 308.5 266.1" style="enable-background:new 0 0 308.5 266.1;" xml:space="preserve"><path id="XMLID_120_" fill="#a2a1a1" d="M231.2,0H77.3L0,133.1l77.3,133.1h153.9l77.3-133.1L231.2,0z M197.7,209.6h-88.5l-43.9-76.6l43.9-76.6h88.5l43.9,76.6l-14.1,24.5l14.1,26.8l-29.7,1.5L197.7,209.6z"></path></svg></span>
            <span class="to-reveal"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 499.8 47.7" style="enable-background:new 0 0 499.8 47.7;" xml:space="preserve">
    <style type="text/css">
        .shapeFill{fill: #a2a1a1;}
    </style>
    <g id="XMLID_101_">
      <polygon id="XMLID_119_" class="shapeFill" points="125,27.9 106.6,0.9 103.3,0.9 103.3,46.5 106.6,46.5 106.6,7 124.7,33.3 125,33.3 
        143.1,7 143.1,46.5 146.4,46.5 146.4,0.9 143.3,0.9   "></polygon>
      <polygon id="XMLID_118_" class="shapeFill" points="177.3,0.7 156.1,46.5 159.6,46.5 165.4,33.7 165.4,33.7 166.8,30.7 166.8,30.7 
        178.9,4.4 192.4,33.7 192.4,33.7 198.3,46.5 202,46.5 180.6,0.7   "></polygon>
      <polygon id="XMLID_117_" class="shapeFill" points="337.9,4.2 337.9,0.9 305.5,0.9 305.5,4.2 308.8,4.2  "></polygon>
      <path id="XMLID_116_" class="shapeFill" d="M35.6,23c-1.4-1.2-3.3-2.1-5.4-2.8c-2.1-0.7-4.4-1.4-7-2.1c-1.6-0.5-2.8-0.7-4-0.9
        c-0.9-0.2-1.9-0.7-2.3-0.9c-0.7-0.2-0.9-0.7-1.2-1.2c-0.2-0.5-0.5-0.9-0.5-1.4v-0.2c0-0.9,0.5-1.6,1.2-2.1c0.7-0.5,1.9-0.9,3.5-0.9
        c2.1,0,4.2,0.5,6.3,1.2c2.1,0.7,4.4,1.9,6.5,3.3l6.3-9.1c-2.6-2.1-5.4-3.5-8.4-4.7c-3-0.5-6.5-0.9-10.5-0.9c-2.8,0-5.1,0.5-7.4,1.2
        c-2.1,0.7-4,1.6-5.6,3C5.6,5.8,4.4,7.2,3.7,9.1c-0.7,1.9-1.2,3.7-1.2,5.8v0.2c0,2.3,0.5,4.2,1.4,5.8c0.9,1.6,2.1,3,3.7,4
        c1.6,1.2,3.3,1.9,5.4,2.6c2.1,0.7,4.4,1.4,6.7,1.9c1.6,0.5,2.8,0.7,3.7,0.9s1.9,0.7,2.3,0.9c0.7,0.5,0.9,0.7,1.2,1.2
        c0.2,0.5,0.2,0.7,0.2,1.2v0.2c0,0.9-0.5,1.9-1.4,2.3c-0.9,0.5-2.1,0.7-4,0.7c-5.4,0-10.2-1.9-14.9-5.6l-7,8.4
       c2.8,2.6,6.1,4.4,9.8,5.8c3.7,1.2,7.4,1.9,11.6,1.9c2.8,0,5.4-0.2,7.7-0.9c2.3-0.7,4.2-1.6,5.8-3c1.6-1.4,2.8-2.8,3.7-4.7
        c0.9-1.9,1.4-4,1.4-6.3v-0.2c0-2.1-0.5-4-1.2-5.6C38.4,25.4,37.2,24,35.6,23z"></path>
      <path id="XMLID_113_" class="shapeFill" d="M87,7c-2.1-2.1-4.7-3.7-7.7-5.1c-3-1.2-6.3-1.9-9.8-1.9c-3.5,0-6.7,0.7-9.8,1.9
        c-3,1.2-5.6,3-7.9,5.1s-4,4.7-5.1,7.7c-1.2,2.8-1.9,6.1-1.9,9.3l0,0c0,3.3,0.7,6.3,1.9,9.3c1.2,2.8,3,5.4,5.1,7.4
        c2.1,2.1,4.7,3.7,7.7,5.1c3,1.2,6.3,1.9,9.8,1.9s6.7-0.7,9.8-1.9c3-1.2,5.6-3,7.9-5.1c2.3-2.1,4-4.7,5.1-7.7c1.2-2.8,1.9-6,1.9-9.3
        v-0.2c0-3.3-0.7-6.3-1.9-9.3C91,11.6,89.1,9.1,87,7z M81.2,24c0,1.6-0.2,3.3-0.9,4.7c-0.5,1.4-1.4,2.8-2.3,4
        c-0.9,1.2-2.3,2.1-3.7,2.6c-1.4,0.7-3,0.9-4.9,0.9c-1.9,0-3.3-0.2-4.9-0.9c-1.4-0.7-2.8-1.6-3.7-2.8c-0.9-1.2-1.9-2.3-2.3-4
        c-0.5-1.4-0.9-3-0.9-4.7v-0.2c0-1.6,0.2-3.3,0.7-4.7c0.5-1.4,1.4-2.8,2.3-4c0.9-1.2,2.3-2.1,3.7-2.6c1.4-0.7,3-0.9,4.7-0.9
        c1.6,0,3.3,0.2,4.9,0.9c1.4,0.7,2.8,1.6,3.7,2.8c1.2,1.2,1.9,2.6,2.3,4C80.7,20.5,81.2,22.1,81.2,24L81.2,24z"></path>
     <polygon id="XMLID_112_" class="shapeFill" points="444,40.7 412.6,0.9 409.3,0.9 409.3,46.5 412.6,46.5 412.6,6.1 444.7,46.5 
        447.3,46.5 447.3,0.9 444,0.9  "></polygon>
     <polygon id="XMLID_111_" class="shapeFill" points="345.8,4.2 361.9,4.2 361.9,46.5 365.1,46.5 365.1,4.2 381.2,4.2 381.2,0.9 345.8,0.9 
          "></polygon>
      <rect id="XMLID_110_" x="391.9" y="0.9" class="shapeFill" width="3.5" height="45.6"></rect>
      <path id="XMLID_109_" class="shapeFill" d="M481.2,23v3.3h15.4v13c-3.3,2.8-8.8,5.1-14.7,5.1c-12.1,0-19.5-8.8-19.5-20.7v-0.2
        c0-10.9,7.7-20.5,18.9-20.5c7,0,11.2,2.3,14.7,5.6l2.3-2.6c-4.7-3.7-9.3-6.1-16.8-6.1c-13.7,0-22.6,11.4-22.6,23.7V24
        c0,12.8,8.6,23.5,23,23.5c7.4,0,13.7-3.3,17.9-6.7V23L481.2,23L481.2,23z"></path>
      <polygon id="XMLID_108_" class="shapeFill" points="296,0.9 291.3,0.9 262.3,30.9 262.3,0.9 258.8,0.9 258.8,46.5 262.3,46.5 262.3,35.1 
        273.4,23.5 292.3,46.5 296.7,46.5 276,21.2   "></polygon>
      <path id="XMLID_103_" class="shapeFill" d="M247.1,14L247.1,14c0-7.7-6.5-13-16.3-13h-19.3v45.6h3.5V27.7h14.9l14.7,18.8h4.4L233.9,27
        C241.3,26.1,247.1,21.6,247.1,14z M215,24.7V4.2h15.6c8.4,0,13,4,13,10v0.2c0,6.5-5.6,10.5-13.3,10.5H215
        C215,24.9,215,24.7,215,24.7z"></path>
      <polygon id="XMLID_102_" class="shapeFill" points="308.8,25.1 335.1,25.1 335.1,22.1 308.8,22.1 305.5,22.1 305.5,46.5 338.4,46.5 
       338.4,43.5 308.8,43.5   "></polygon>
    </g>
    </svg></span>
    </a>
    <?php 
  	die();
}
