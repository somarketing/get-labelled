<?php 


function so_enquiryform() {
    $html = '';
    $html .= '<form class="form so_form" id="enquiry-form" action="grecaptcha.execute">';
        $html .= '<div class="input-group">';
            $html .= '<label>First Name*</label>';
            $html .= '<input type="text" value="" name="first_name" required>';
        $html .= '</div>';
        $html .= '<div class="input-group">';
            $html .= '<label>Last Name*</label>';
            $html .= '<input type="text" value="" name="last_name" required>';
        $html .= '</div>';
        $html .= '<div class="input-group">';
            $html .= '<label>Telephone</label>';
            $html .= '<input type="tel" value="" name="telephone">';
        $html .= '</div>';
        $html .= '<div class="input-group">';
            $html .= '<label>Email*</label>';
            $html .= '<input type="email" value="" name="email" required>';
        $html .= '</div>';
        $html .= '<div class="input-group enquiry">';
            $html .= '<label>Message*</label>';
            $html .= '<textarea required name="message"></textarea>';
        $html .= '</div>';
        $html .= '<div class="input-group terms_agree">';
            $html .= '<small>* Required Fields</small>';
            $html .= '<p>By completing this form I agree that my data will be held only for the purposes of replying to this enquiry. My data will not be shared with any third parties or used for marketing purposes thereafter.</p><p>We will only use the data you have submitted to contact you in response to this form in accordance with our <a href="/privacy/" target="_blank" title="Privacy Policy">privacy policy</a></p>';
            $html .= '<label class="checkbox">';
                $html .= '<input type="checkbox" name="GDPR_privacy" required/>';
                $html .= '<span> You must agree to our <a target="_blank" href="/privacy/" title="Privacy Policy">privacy policy</a></span>';
                
            $html .= '</label>';
            $html .= '<label class="checkbox">';
                $html .= '<input type="checkbox" name="terms_conditions" required/> ';
                $html .= '<span>You have read and agree to our <a target="_blank" href="/terms-conditions/">terms and conditions</a>.</span>';
            $html .= '</label>';

            $html .='<input type="hidden" name="subject" value="Enquiry Form" />';
            $html .= '<div class="submit_wrap">';
                $html .= '<div class="alert"></div>';
                
            $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="input-group submit">';
            $html .= '<button class="ajs_btn submit" data-sitekey="6LdKBt4UAAAAAE3z54yJGuwFyi-2q6QMaCsR28wU" data-callback="onSubmit">Send Enquiry</button>';
        $html .='</div>';
    $html .= '</form>';

    return $html;
}

add_action( 'wp_ajax_enquiry_form_submit', 'enquiry_form_submit' );
add_action( 'wp_ajax_nopriv_enquiry_form_submit', 'enquiry_form_submit' );

function enquiry_form_submit() {
    global $options;
    
    $logo = get_template_directory_uri().'/img/logo.png';
    $formData = $_POST['form_data'];
    
    ob_start();
    $data = explode('&',$formData);
    $data = array_map('urldecode',$data);

    $formValues = array();
    foreach($data as $d) {
        $x = explode('=',$d);
        $label = $x[0];
        $clean_label = str_replace('_',' ',$label);
        $clean_label = ucfirst($clean_label);
        $value = $x[1];

        $formValues[$label] = array($clean_label,$value);
    }

    if(locate_template('so-form/email-enquiry.php')) {
        include(locate_template('so-form/email-enquiry.php'));
    } else {
        $html = '';
        $html .= '<table style="width: 100%;">';
        foreach($formValues as $field) {
            $html .= '<tr>';
                $html .='<th align="left">'.$field[0].'</th>';
                $html .='<td>'.$field[1].'</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        echo $html;
    }
    $message = ob_get_clean();

    $headers = "From: \"Enquiry Form\" <".$options['from_email'].">\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


    $to = 'arran@somarketing.co.uk';
    if (class_exists('acf')) {
        $to = get_field('form_email','option');
    }
    $subject = $formValues['subject'][1];

    wp_mail($to, $subject, $message, $headers);

    if ( isset($formValues) ) {
        if ($formValues) {
            wp_insert_post(
                array(
                    'post_title'  => $formValues['subject'][1].' - '.$formValues['first_name'][1],
                    'post_type'   => 'submission',
                    'post_status' => 'draft', /* Or "draft", if required */
                    'post_content'=> $html
                )       
            );
        }
    } 

    
    $html = '';
    $html .='<div class="form-success">';
        $html .='<header><h2>Thank You</h2></header>';
        $html .='<section><h3>Thank you for your enquiry,<br /> a member of our team will contact you shortly.</h3></section>';
    $html .='</div>';
    
    echo $html;
    
    die();
}

function fallback_submit() {

}


add_action( 'init', 'submission_init' );

function submission_init() {
    $labels = array(
        'name'               => _x( 'Submissions', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Submission', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Submissions', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Submissions', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'book', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New submission', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New submission member', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit submission', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View submission', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All submission', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search submission', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent submission:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No submissions found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No submissions found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => false,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'submission' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'page-attributes','editor')
    );

    register_post_type( 'submission', $args );
}



add_action( 'wp_ajax_newsletter_form_pop_submit', 'newsletter_form_pop_submit' );
add_action( 'wp_ajax_nopriv_newsletter_form_pop_submit', 'newsletter_form_pop_submit' );
function newsletter_form_pop_submit() {
    $options = get_fields('options');
    $html = '';

    $newsletter_content = get_field('newsletter_content','options');

    if($newsletter_content['display_popup'] == true) {
        
        $title = $newsletter_content['title'];
        $content = $newsletter_content['content'];

        if($newsletter_content['background']['url']) {
            $class = 'image';
            $background = 'background-image: url('.$newsletter_content['background']['url'].'); ';
        }
        
        $html .= '<div id="enquiry-form-confirmation">';
            $html .= '<div>';
                $html .= '<button class="closer"><strong>Close</strong> <span class="fa fa-times"></span></button>';
                $html .= '<header class="title '.$class.'" style="'.$background.'"><h2><strong>'.$title.'</strong></h2></header>';
                $html .= '<div class="wysiwyg" style="text-align: center;">'.$content.'</div>';
                $html .= '<div id="mc_embed_signup"><form action="'.$options['api_mailchimp'].'" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank">';
                    $html .= '<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Please enter your email" required>';
                    $html .= '<small>* required fields</small>';
                    $html .= '<div class="input-group">';
                        $html .= '<p>You agree to allow '.get_bloginfo('name').' to contact you at the provided email address for marketing and promotional purposes as laid out in our <a href="/privacy/" title="Privacy Policy">privacy policy</a> <br /><strong>we will not share or sell your data to any third parties.</strong></p>';
                        $html .= '<label class="checkbox">';
                            $html .= '<input type="checkbox" name="GDPR_privacy" required/>';
                            $html .= '<span>You must agree to our <a href="/privacy/" title="Privacy Policy">privacy policy</a></span>';
                            
                        $html .= '</label>';
                        $html .= '<label class="checkbox">';
                            $html .= '<input type="checkbox" name="terms_conditions" required/>';
                            $html .= '<span>You have read and agree to our <a href="/terms-of-use/">terms and conditions</a>.</span>';
                        $html .= '</label>';
                    $html .= '</div>';
                    $html .= '<div class="response" id="mce-error-response" style="display:none"></div><div class="response" id="mce-success-response" style="display:none"></div>';
                    $html .= '<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_fa59a70fd5_0eb0b047f6" tabindex="-1" value=""></div>';
                    $html .='<input type="hidden" name="subject" value="Enquiry Form" />';
                    $html .= '<div class="submit_wrap">';
                        $html .= '<div class="alert"></div>';
                        $html .= '<button class="submit btn" data-callback="onSubmit">Send Enquiry</button>';
                    $html .= '</div>';
                $html .= '</div>';
                $html .= '</form></div>';
            $html .= '</div>';
        $html .= '</div>';
        $html .= '<script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js"></script><script type="text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]=\'EMAIL\';ftypes[0]=\'email\';fnames[1]=\'FNAME\';ftypes[1]=\'text\';fnames[2]=\'LNAME\';ftypes[2]=\'text\';fnames[9]=\'MMERGE9\';ftypes[9]=\'text\';fnames[10]=\'CGROUP\';ftypes[10]=\'dropdown\';fnames[4]=\'MMERGE4\';ftypes[4]=\'radio\';fnames[5]=\'MMERGE5\';ftypes[5]=\'date\';fnames[13]=\'DOB\';ftypes[13]=\'birthday\';}(jQuery));var $mcj = jQuery.noConflict(true);</script>';
    }

    echo $html;
    die();
}

