<?php
add_action( 'wp_ajax_product_count', 'product_count' );
add_action( 'wp_ajax_nopriv_product_count', 'product_count' );
  
function product_count() {
	$response = array();
	$response['cart_count'] = '';
	if ( class_exists( 'WooCommerce' ) ) {
	
		$html = '';
		if(WC()->cart->get_cart_contents_count() > 0) {
			$html .= '<span class="count">'.WC()->cart->get_cart_contents_count().'</span>';
		} else {
			$html .= '';
		}
		$response['cart_count'] = $html;

		$response = json_encode($response);
		
	}
	echo $response;
	die();
}