<?php 


add_action( 'woocommerce_before_shop_loop', 'ps_selectbox', 25 );
function ps_selectbox() {
    $per_page = filter_input(INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT);     
    echo '<div class="woocommerce-perpage">';
        echo '<div class="custom-select">';
            echo '<select onchange="if (this.value) window.location.href=this.value">';   
            $orderby_options = array(
                '12' => '12',
                '12' => '12',
                '16' => '16',
                '32' => '32',
                '64' => '64'
            );
            foreach( $orderby_options as $value => $label ) {
                echo "<option ".selected( $per_page, $value )." value='?perpage=$value'>$label Per Page</option>";
            }
            echo '</select>';
        echo '</div>';
    echo '</div>';
}

add_action( 'pre_get_posts', 'ps_pre_get_products_query' );
function ps_pre_get_products_query( $query ) {
   $per_page = filter_input(INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT);
   // echo get_post_type_archive_link('product');
   if( $query->is_main_query() && !is_admin()){
        $query->set( 'posts_per_page', $per_page );
    }
}


remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0); 

remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

add_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display', 50 );



function qty_btns() {
    $html = '';
    $html .= '<button class="fa fa-minus" aria-label="Decrease quantity" aria-hidden="true"></button>';
    $html .= '<button class="fa fa-plus" aria-label="Increase quantity" aria-hidden="true"></button>';
    echo $html;
}
add_action( 'woocommerce_after_quantity_input_field', 'qty_btns' );

//product tabs

/**
 * Add a custom product tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
    
    // Adds the new tab
    unset( $tabs['additional_information']);
    unset( $tabs['reviews']);    
    // $tabs['reviews']['priority'] = 52;
    
    $tabs['description'] = array(
        'title'     => __( 'Description', 'woocommerce' ),
        'priority'  => 47,
        'callback'  => 'woo_description_content'
    );
    $tabs['delivery'] = array(
        'title'     => __( 'Delivery', 'woocommerce' ),
        'priority'  => 51,
        'callback'  => 'woo_delivery_content'
    );
    $tabs['additional_info'] = array(
        'title'     => __( 'Additional Information', 'woocommerce' ),
        'priority'  => 51,
        'callback'  => 'woo_additional_info'
    );

    return $tabs;
}


function woo_delivery_content() {
    global $product;
    $prod_id = $product->id;
    $html = get_field('delivery','cpt_product');
    if(get_field('delivery',$prod_id)) {
        $html = get_field('delivery',$prod_id);
    }
    return $html;
}

function woo_description_content() {
    global $product;
    $prod_id = $product->id;
    $html = '';
    if(get_field('description',$prod_id)) {
        $html = get_field('description',$prod_id);
    }
    return $html;
}

function woo_additional_info() {
    global $product;
    // do_action( 'woocommerce_product_additional_information', $product );
}




// Change Price Filter Widget Increment
function change_price_filter_step() {
        return 1;
}
add_filter( 'woocommerce_price_filter_widget_step', 'change_price_filter_step', 10, 3 );

function md_custom_woocommerce_checkout_fields( $fields ) {
    $fields['order']['order_comments']['placeholder'] = 'Instructions for your delivery driver.';
    $fields['order']['order_comments']['label'] = 'Delivery Instructions';

    return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'md_custom_woocommerce_checkout_fields' );

add_action( 'wpo_wcpdf_after_order_details', 'bbloomer_add_note_to_customer_to_PDF_invoice_new', 10, 2 );
 
function bbloomer_add_note_to_customer_to_PDF_invoice_new( $template_type, $order ) {
    $document = wcpdf_get_document( $template_type, $order );
    $document->order_notes();
}

function attribute_icons($sidebar, $attributes, $position) {
    $qry = get_queried_object();
    if ($position == "after") {
        foreach ($sidebar['pre'] as $attribute) {
            unset($attributes[$attribute]);
        }
        array_multisort($sidebar['post'], $attributes);
    } elseif ($position == "before") {
        foreach ($sidebar['post'] as $attribute) {
            unset($attributes[$attribute]);
        }
        array_multisort($sidebar['pre'], $attributes);
    }
    foreach ($attributes as $key => $attribute) {
        $attr_terms = get_terms('pa_' . $key); ?>
        <li class="widget woocommerce widget_layered_nav woocommerce-widget-layered-nav">
            <?php echo '<h3 class="widget-title">' . $attribute['label'] . "</h3>"; ?>
            <ul class="woocommerce-widget-layered-nav-list">
                <?php foreach ($attr_terms as $term) { ?>

                    <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term <?php if($_GET[$filter] == $term->slug) { echo "woocommerce-widget-layered-nav-list__item--chosen chosen"; } ?>">
                        <!-- <a href="<?php echo get_term_link($qry->term_id) . '?filter_' . $key . '=' . $term->slug; ?>"><?php echo $term->name ?></a> -->
                        <?php
                        $filter = 'filter_' . $key;
                        if (!empty($_GET[$filter]) && $_GET[$filter] == $term->slug) {
                            $url = remove_query_arg($filter, get_nopaging_url());
                        } else {
                            $url = add_query_arg( $filter, $term->slug, get_nopaging_url() );
                        } ?>
                        <a href="<?php echo $url ?>" title="<?php echo $term->name; ?>"><?php echo $term->name; ?></a>
                    </li>
                <?php } ?>
            </ul>
        </li>
    <?php }
}

function ajs_stockstatus() {
    global $product; ?>
        <p class="stock-status in-stock">
            <?php
            switch ($product->get_stock_status()) {
                case 'instock':
                    echo '<span class="fa fa-check-circle"></span> In Stock';
                    break;
                default:
                    echo '<span class="fa fa-times-circle"></span> Out of Stock';
                    break;
            }
            ?>
        </p>
    <?php
}

function disable_woo_commerce_sidebar() {
    remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10); 
}
add_action('init', 'disable_woo_commerce_sidebar');

function ajs_attribute($featured = false) {
    global $product;
    $attributes = $product->get_attributes();

    $display_result = '<dl class="attributes">';
    foreach ( $attributes as $attribute ) {


        if ( $attribute->get_variation() ) {
            continue;
        }
        $name = $attribute->get_name();
        $attr_id = $attribute->get_id();
        // print_r(get_option( "wc_attribute_featured-$attr_id" ));
        if($featured == true) {
            if(get_option( "wc_attribute_featured-$attr_id" ) == 'yes') {
                if ( $attribute->is_taxonomy() ) {

                    $terms = wp_get_post_terms( $product->get_id(), $name, 'all' );

                    $cwtax = $terms[0]->taxonomy;

                    $cw_object_taxonomy = get_taxonomy($cwtax);

                    if ( isset ($cw_object_taxonomy->labels->singular_name) ) {
                        $tax_label = $cw_object_taxonomy->labels->singular_name;
                    } elseif ( isset( $cw_object_taxonomy->label ) ) {
                        $tax_label = $cw_object_taxonomy->label;
                        if ( 0 === strpos( $tax_label, 'Product ' ) ) {
                            $tax_label = substr( $tax_label, 8 );
                        }
                    }
                    $display_result .= '<div class="attribute">';
                    $display_result .= '<dt>'.$tax_label . ':</dt> ';
                    $tax_terms = array();
                    foreach ( $terms as $term ) {
                        $single_term = esc_html( $term->name );
                        array_push( $tax_terms, $single_term );
                    }
                    $display_result .= '<dd>'.implode(', ', $tax_terms) .  '</dd>';
                    $display_result .= '</div>';
                } else {
                    $display_result .= $name . ': ';
                    $display_result .= esc_html( implode( ', ', $attribute->get_options() ) ) . '<br />';
                }
            }
        } else {
            if ( $attribute->is_taxonomy() ) {

                $terms = wp_get_post_terms( $product->get_id(), $name, 'all' );

                $cwtax = $terms[0]->taxonomy;

                $cw_object_taxonomy = get_taxonomy($cwtax);

                if ( isset ($cw_object_taxonomy->labels->singular_name) ) {
                    $tax_label = $cw_object_taxonomy->labels->singular_name;
                } elseif ( isset( $cw_object_taxonomy->label ) ) {
                    $tax_label = $cw_object_taxonomy->label;
                    if ( 0 === strpos( $tax_label, 'Product ' ) ) {
                        $tax_label = substr( $tax_label, 8 );
                    }
                }
                $display_result .= '<div class="attribute">';
                $display_result .= '<dt>'.$tax_label . ':</dt> ';
                $tax_terms = array();
                foreach ( $terms as $term ) {
                    $single_term = esc_html( $term->name );
                    array_push( $tax_terms, $single_term );
                }
                $display_result .= '<dd>'.implode(', ', $tax_terms) .  '</dd>';
                $display_result .= '</div>';
            } else {
                $display_result .= $name . ': ';
                $display_result .= esc_html( implode( ', ', $attribute->get_options() ) ) . '<br />';
            }
        }
    }
    $display_result .= '</dl>';

    return $display_result;
}

function action_woocommerce_before_edit_attribute_fields(  ) {
    $id = isset( $_GET['edit'] ) ? absint( $_GET['edit'] ) : 0;

    $value = $id ? get_option( "wc_attribute_display_type-$id" ) : '';
    $options = array('dropdown','radio','color');
    ?>
    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="display_type"><?php _e('Display Type', ''); ?></label>
        </th>
        <td>
            <select name="display_type" required>
                <option value="select" disabled selected>Select</option>
                <?php foreach($options as $option) {
                    $selected = '';
                    if($option == $value) $selected = 'selected';
                    echo '<option '.$selected.' value="'.$option.'">'.ucfirst($option).'</option>';
                } ?>
            </select>
        </td>
    </tr>
    <?php
    $value = $id ? get_option( "wc_attribute_featured-$id" ) : '';
    $options = array('no','yes');
    ?>
    <tr class="form-field">

        <th valign="top" scope="row">
            <label for="featured"><?php _e('Featured', ''); ?></label>
        </th>
        <td>
            <select name="featured" required>
                <?php foreach($options as $option) {
                    $selected = '';
                    if($option == $value) $selected = 'selected';
                    echo '<option '.$selected.' value="'.$option.'">'.ucfirst($option).'</option>';
                } ?>
            </select>
        </td>
    </tr>
    <?php
}
add_action( 'woocommerce_before_edit_attribute_fields', 'action_woocommerce_before_edit_attribute_fields', 10, 0 );

function my_save_wc_attribute_display_type( $id ) {
    if ( is_admin() && isset( $_POST['display_type'] ) ) {
        $option = "wc_attribute_display_type-$id";
        update_option( $option, sanitize_text_field( $_POST['display_type'] ) );
    }
}
add_action( 'woocommerce_attribute_added', 'my_save_wc_attribute_display_type' );
add_action( 'woocommerce_attribute_updated', 'my_save_wc_attribute_display_type' );

function my_save_wc_attribute_featured( $id ) {
    if ( is_admin() && isset( $_POST['featured'] ) ) {
        $option = "wc_attribute_featured-$id";
        update_option( $option, sanitize_text_field( $_POST['featured'] ) );
    }
}
add_action( 'woocommerce_attribute_added', 'my_save_wc_attribute_featured' );
add_action( 'woocommerce_attribute_updated', 'my_save_wc_attribute_featured' );

add_action( 'woocommerce_attribute_deleted', function ( $id ) {
    delete_option( "wc_attribute_display_type-$id" );
    delete_option( "wc_attribute_featured-$id" );
} );