<?php

// function add_logo($items,$args) {
  
//   $logo = '<img src="'.get_template_directory_uri().'/img/logo.svg'.'" alt="'.get_bloginfo('name').'" width="143px" height="144px" />';

//   $branding = '';

//   $branding .= '<li id="branding"><a href="/" title="'.get_bloginfo('name').'">';
//     $branding .= $logo;
//   $branding .= '</a></li>';

//   return $items .= $branding;
// }
// add_filter('wp_nav_menu_main-menu_items','add_logo', 10, 2 );

class header_walker extends Walker_Nav_Menu {

  function start_lvl(&$output, $depth = 0, $args = array()) {
        $output .= '<div><ul>';
  }
  function end_lvl(&$output, $depth = 0, $args = array()) {
    $output .= '</ul></div>';
  }
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="'. esc_attr( $class_names ) . ' depth-'.$depth.'"';

        
        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        $prepend = '<span>';
        $append = '</span>';
        $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

        if($depth != 0) {
          $description = $append = $prepend = "";
        }

        $icon = get_field('icon', $item);
        $iconHTML = '';
        if($icon) $iconHTML = '<img data-src="'.$icon['url'].'" title="'.$icon['title'].'" />';

        $item_output = $args->before;
        $item_output .= '<a class="ajax" data-id="'.$item->ID.'" '. $attributes .'>'.$iconHTML;
        $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
        $item_output .= $description.$args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
    function end_el(&$output, $item, $depth = 0, $args = array()) {
      $output .= "</li>";
    }
}


function clean_custom_menus($slug,$span = 1) {
  $menu_name = $slug; // specify custom menu slug
  if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
    $menu = wp_get_nav_menu_object($locations[$menu_name]);
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    // print_r($menu);
    $menu_list = '<dl class="menu '.$menu->slug.' span-'.$span.'">' ."\n";
      $menu_list .= '<dt class="title">'.$menu->name.'</dt>';
      foreach ((array) $menu_items as $key => $menu_item) {
        $title = $menu_item->title;
        $url = $menu_item->url;
        // print_r($menu_item);
        $menu_list .= '<dd><a href="'. $url .'" title="'. $title .'">'. $title .'</a></dd>';
      }
    $menu_list .= "\t\t\t". '</dl>' ."\n";
  } else {
    // $menu_list = '<!-- no list defined -->';
  }
  return $menu_list;
}
