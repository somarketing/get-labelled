<?php

add_filter( 'excerpt_length', function($length) {
    return 20;
} );

//Cleaner Titles
function ajsTitle() {
	if((is_page()) || (is_singular())) {
		$title = get_the_title();
	}
	if(is_home() || (is_single())) {
		$title = 'Thoughtful news';
	}
	if(is_archive()) {
		$title = post_type_archive_title('',false);
	}
	if((is_category()) || (is_tax())) {
		$title = single_term_title('',false);
	}
    if(is_404()) {
        $title = '404';
    }
    if(is_woocommerce()) {
        $title = woocommerce_page_title(false);
    }
	return $title;
}

// Make a string into a phone number
function phoneURL($string) {
	//Lower case everything
	$string = strtolower($string);
	//Make alphanumeric (removes all other characters)
	$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
	//Clean up multiple dashes or whitespaces
	$string = preg_replace("/[\s-]+/", " ", $string);
	//Convert whitespaces and underscore to dash
	$string = preg_replace("/[\s_]/", "+", $string);
	return $string;
}

function remove_http($url) {
   $disallowed = array('http://', 'https://');
   foreach($disallowed as $d) {
      if(strpos($url, $d) === 0) {
         return str_replace($d, '', $url);
      }
   }
   return $url;
}


//excerpt more
function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


/*=============================================
=            BREADCRUMB 			          =
=============================================*/
//  to include in functions.php
function the_breadcrumb() {
    $sep = ' > ';
    if (!is_front_page()) {
	
	// Start the breadcrumb with a link to your homepage
        echo '<nav class="breadcrumb"><ul>';
        echo '<li><a href="';
        echo get_option('home');
        echo '" title="Home">Home</a></li>';
	
	// Check if the current page is a category, an archive or a single page. If so show the category or archive name.
        if (is_category() || is_single() ){
            echo '<li>';
                	the_category('title_li=');
            echo '</li>';
        } elseif (is_archive() || is_single()){
            if ( is_day() ) {
                echo '<li>';
                	printf( __( '%s', 'text_domain' ), get_the_date() );
                echo '</li>';
            } elseif ( is_month() ) {
            	echo '<li>';
                	printf( __( '%s', 'text_domain' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'text_domain' ) ) );
                echo '</li>';
            } elseif ( is_year() ) {
            	echo '<li>';
                	printf( __( '%s', 'text_domain' ), get_the_date( _x( 'Y', 'yearly archives date format', 'text_domain' ) ) );
                echo '</li>';
            } else {
            	echo '<li>';
                	_e( 'Blog Archives', 'text_domain' );
                echo '</li>';
            }
        }
	
	// If the current page is a single post, show its title with the separator
	       if (is_single()) {
 	           echo '<li>';
 	              the_title();
               echo '</li>';
 	       }
	
	// If the current page is a static page, show its title.
        if (is_page()) {
            echo '<li>'.the_title().'</li>';
        }
	
	// if you have a static page assigned to be you posts list page. It will find the title of the static page and display it. i.e Home >> Blog
        if (is_home()){
            global $post;
            $page_for_posts_id = get_option('page_for_posts');
            if ( $page_for_posts_id ) { 
                $post = get_page($page_for_posts_id);
                setup_postdata($post);
                echo '<li>'.get_the_title().'</li>';
                rewind_posts();
            }
        }
        echo '</ul></nav>';
    }
}


/*=============================================
=            PAGINATION                      =
=============================================*/
/* Adds pagination function */
function pagination() {
    if( is_singular() )
        return;
    global $wp_query;
    /* Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
    /* Add current page to the array */
    if ( $paged >= 1 ) {
        $links[] = $paged;
    }
    /*  Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
    echo '<nav class="pagination"><ul>' . "\n";
    /*  Previous Post Link */
    if ( get_previous_posts_link() ) {
        printf( '<li class="first">%s</li>' . "\n", get_previous_posts_link('&#8592;') );
    } 
    /*  Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="current"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
        if ( ! in_array( 2, $links ) )
            echo '<li class="eli">…</li>' . "\n";
    }
    /* Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="current"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
    /*  Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li class="eli">…</li>' . "\n";
            $class = $paged == $max ? ' class="current"' : '';
            printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
        }
    /*  Next Post Link */
    if ( get_next_posts_link() ) {
        printf( '<li class="last">%s</li>' . "\n", get_next_posts_link('&#8594;') );
    } 
    echo '</ul></nav>' . "\n";
}


/*=============================================
=            CMS Row builder			  	  =
=============================================*/
function ajsCMS($flex,$id = '') {
	if( have_rows($flex, $id) ):
	    while ( have_rows($flex, $id) ) : the_row();
	    	include(get_template_directory().'/cms/'.get_row_layout().'.php');
	    endwhile;
	else :
	    // no layouts found
	endif;
}

/*=============================================
=            Shortcodes 				  	  =
=============================================*/
//	Button
function button( $atts, $content = null ) {
	$a = shortcode_atts( array(
		'link' => '/contact-us',
		'text' => 'Contact Us',
	), $atts );
	return '<a class="btn" href="'.$a['link'].'">'.$a['text'].'</a>';
}
add_shortcode('button', 'button');
