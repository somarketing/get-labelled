<?php

add_action('admin_head', 'my_custom_styles');
function my_custom_styles() {
  echo '<style>
  	@import url("https://p.typekit.net/p.css?s=1&k=wbr3jik&ht=tk&f=10294.10296.10298.10300.10302.16353.37461.37466.37467&a=2189344&app=typekit&e=css");


  		@font-face {
		font-family:"montserrat";
		src:url("https://use.typekit.net/af/2d988a/00000000000000003b9b1338/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3") format("woff2"),url("https://use.typekit.net/af/2d988a/00000000000000003b9b1338/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3") format("woff"),url("https://use.typekit.net/af/2d988a/00000000000000003b9b1338/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3") format("opentype");
		font-display:swap;font-style:normal;font-weight:400;
	}

	@font-face {
	font-family:"montserrat";
	src:url("https://use.typekit.net/af/05093b/00000000000000003b9b133d/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3") format("woff2"),url("https://use.typekit.net/af/05093b/00000000000000003b9b133d/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3") format("woff"),url("https://use.typekit.net/af/05093b/00000000000000003b9b133d/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3") format("opentype");
	font-display:swap;font-style:normal;font-weight:700;
	}

	@font-face {
	font-family:"montserrat";
	src:url("https://use.typekit.net/af/1bab1a/00000000000000003b9b133e/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i7&v=3") format("woff2"),url("https://use.typekit.net/af/1bab1a/00000000000000003b9b133e/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i7&v=3") format("woff"),url("https://use.typekit.net/af/1bab1a/00000000000000003b9b133e/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i7&v=3") format("opentype");
	font-display:swap;font-style:italic;font-weight:700;
	}

	@font-face {
	font-family:"montserrat";
	src:url("https://use.typekit.net/af/ed6ac1/00000000000000003b9b1341/27/l?subset_id=2&fvd=n9&v=3") format("woff2"),url("https://use.typekit.net/af/ed6ac1/00000000000000003b9b1341/27/d?subset_id=2&fvd=n9&v=3") format("woff"),url("https://use.typekit.net/af/ed6ac1/00000000000000003b9b1341/27/a?subset_id=2&fvd=n9&v=3") format("opentype");
	font-display:swap;font-style:normal;font-weight:900;
	}

	.tk-brandon-grotesque { font-family: "brandon-grotesque",sans-serif; }
	.tk-montserrat { font-family: "montserrat",sans-serif; }

    body, td, textarea, input, select,.mce-content-body {
      font-family: "montserrat";
      font-size: 13px;
      font-weight: 400;
    }

    .acf-row:nth-child(even) .acf-fields {
    	background: #f3f3f3;
    }

    .acf-row .acf-fields:hover {
    	outline-offset: -5px;
    	outline: 4px solid #ffb200 !important;
    	background: #f8faf8;
    }

  </style>';
}

/**
 * Registers an editor stylesheet for the theme.
 */
function wpdocs_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );

/*remove term descriptions from post editor */

function wpse_hide_cat_descr() { ?>

    <style type="text/css">
       .term-description-wrap {
           display: none;
       }
    </style>

<?php } 

add_action( 'admin_head-term.php', 'wpse_hide_cat_descr' );
add_action( 'admin_head-edit-tags.php', 'wpse_hide_cat_descr' );
