<?php 
/**
 * Add svg support
 *
 */
add_filter( 'wp_check_filetype_and_ext', function( $data, $file, $filename, $mimes) {
      global $wp_version;
      if( $wp_version == '4.7' || ( (float) $wp_version < 4.7 ) ) {
      return $data;
    }
    $filetype = wp_check_filetype( $filename, $mimes );
      return [
      'ext'             => $filetype['ext'],
      'type'            => $filetype['type'],
      'proper_filename' => $data['proper_filename']
    ];
}, 10, 4 );

function ns_mime_types( $mimes ){
   $mimes['svg'] = 'image/svg+xml';
   return $mimes;
}
add_filter( 'upload_mimes', 'ns_mime_types' );

function ns_fix_svg() {
  echo '<style type="text/css">.attachment-266x266, .thumbnail img { width: 100% !important; height: auto !important;} </style>';
}
add_action( 'admin_head', 'ns_fix_svg' );


function imageSetup() {
	function image_crop_dimensions($default, $orig_w, $orig_h, $new_w, $new_h, $crop){
	    if ( !$crop ) return null; // let the wordpress default function handle this

	    $aspect_ratio = $orig_w / $orig_h;
	    $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

	    $crop_w = round($new_w / $size_ratio);
	    $crop_h = round($new_h / $size_ratio);

	    $s_x = floor( ($orig_w - $crop_w) / 2 );
	    $s_y = floor( ($orig_h - $crop_h) / 2 );

	    return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
	}

	add_filter('image_resize_dimensions', 'image_crop_dimensions', 10, 6);

    add_theme_support( 'post-thumbnails' );

    //hero desktop
	$dimensions = array(
		'x' =>	1920,
		'y' => 	925
	);
    add_image_size( 'hero-desktop-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'hero-desktop',$dimensions['x'],$dimensions['y'],true);

    //hero tablet
	$dimensions = array(
		'x' =>	1024,
		'y' => 	925
	);
    add_image_size( 'hero-tablet-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'hero-tablet',$dimensions['x'],$dimensions['y'],true);

    //hero mobile
	$dimensions = array(
		'x' =>	768,
		'y' => 	925
	);
    add_image_size( 'hero-mobile-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'hero-mobile',$dimensions['x'],$dimensions['y'],true);

    //product
	$dimensions = array(
		'x' =>	560,
		'y' => 	800
	);
    add_image_size( 'product-main-2x',$dimensions['x']*2,$dimensions['y']*2,false);
    add_image_size( 'product-main',$dimensions['x'],$dimensions['y'],false);

    //product background
	$dimensions = array(
		'x' =>	760,
		'y' => 	671
	);
    add_image_size( 'product-background-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'product-background',$dimensions['x'],$dimensions['y'],true);

    //product thumbs
	$dimensions = array(
		'x' =>	760,
		'y' => 	760
	);
    add_image_size( 'product-thumb-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'product-thumb',$dimensions['x'],$dimensions['y'],true);

    //product thumbs
	$dimensions = array(
		'x' =>	1640,
		'y' => 	198
	);
    add_image_size( 'page-head-desktop-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'page-head-desktop',$dimensions['x'],$dimensions['y'],true);

    //product thumbs
	$dimensions = array(
		'x' =>	1024,
		'y' => 	198
	);
    add_image_size( 'page-head-tablet-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'page-head-tablet',$dimensions['x'],$dimensions['y'],true);

    //product thumbs
	$dimensions = array(
		'x' =>	768,
		'y' => 	708
	);
    add_image_size( 'page-head-mobile-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'page-head-mobile',$dimensions['x'],$dimensions['y'],true);

    //product thumbs
	$dimensions = array(
		'x' =>	1640,
		'y' => 	198
	
	);
    add_image_size( 'shop-head-desktop-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'shop-head-desktop',$dimensions['x'],$dimensions['y'],true);

    //product thumbs
	$dimensions = array(
		'x' =>	1024,
		'y' => 	198
	);
    add_image_size( 'shop-head-tablet-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'shop-head-tablet',$dimensions['x'],$dimensions['y'],true);

    //product thumbs
	$dimensions = array(
		'x' =>	768,
		'y' => 	396
	);
    add_image_size( 'shop-head-mobile-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'shop-head-mobile',$dimensions['x'],$dimensions['y'],true);

    
	$dimensions = array(
		'x' =>	800,
		'y' => 	600
	);
    add_image_size( 'photo-2x',$dimensions['x']*2,$dimensions['y']*2,true);
    add_image_size( 'photo',$dimensions['x'],$dimensions['y'],true);

	$dimensions = array(
		'x' =>	500,
		'y' => 	500
	);
    add_image_size( 'product-box-2x',$dimensions['x']*2,$dimensions['y']*2,false);
    add_image_size( 'product-box',$dimensions['x'],$dimensions['y'],false);

  $dimensions = array(
		'x' =>	808,
		'y' => 	808
	);
  add_image_size( 'category-2x',$dimensions['x']*2,$dimensions['y']*2,true);
  add_image_size( 'category',$dimensions['x'],$dimensions['y'],true);

  $dimensions = array(
		'x' =>	808,
		'y' => 	300
	);
  add_image_size( 'banner-2x',$dimensions['x']*2,$dimensions['y']*2,true);
  add_image_size( 'banner',$dimensions['x'],$dimensions['y'],true);

  $dimensions = array(
		'x' =>	187,
		'y' => 	108
	);
  add_image_size( 'brand-2x',$dimensions['x']*2,$dimensions['y']*2,false);
  add_image_size( 'brand',$dimensions['x'],$dimensions['y'],false);


}
add_action( 'after_setup_theme', 'imageSetup' );


function ajsImage($id = '',$size = '', $lazy = 'lazy') {

	if($id == '') {
	 	$id = get_post_thumbnail_id($post->ID);
	}

	if($size !== '') {
		if(has_image_size($size.'-2x')) {
			$size = $size.'-2x';
		}
	}

	if(wp_get_attachment_image_src($id,$size)[1] && wp_get_attachment_image_src($id,$size)[2]) {
		$width = wp_get_attachment_image_src($id,$size)[1];
		$height = wp_get_attachment_image_src($id,$size)[2];
		$ratio = $width / $height;
	}
	
	// print_r($ratio);
	$class = $size;
	if($ratio < 1) {
		$class .= ' tall-image';
	}
	$attr = array( 
		'class' 	=> $class,
		'loading'	=>  $lazy
	);
	$html = wp_get_attachment_image($id,$size,false,$attr);

	return $html;
}
