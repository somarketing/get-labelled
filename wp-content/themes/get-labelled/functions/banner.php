<?php 

add_action( 'init', 'banner_init' );
/**
 * Register a team post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function banner_init() {
  $labels = array(
    'name'               => _x( 'Banners', 'post type general name', 'your-plugin-textdomain' ),
    'singular_name'      => _x( 'Banner', 'post type singular name', 'your-plugin-textdomain' ),
    'menu_name'          => _x( 'Banners', 'admin menu', 'your-plugin-textdomain' ),
    'name_admin_bar'     => _x( 'Banner', 'add new on admin bar', 'your-plugin-textdomain' ),
    'add_new'            => _x( 'Add New', 'book', 'your-plugin-textdomain' ),
    'add_new_item'       => __( 'Add New Banner', 'your-plugin-textdomain' ),
    'new_item'           => __( 'New banner', 'your-plugin-textdomain' ),
    'edit_item'          => __( 'Edit banner', 'your-plugin-textdomain' ),
    'view_item'          => __( 'View banner', 'your-plugin-textdomain' ),
    'all_items'          => __( 'All banners', 'your-plugin-textdomain' ),
    'search_items'       => __( 'Search banners', 'your-plugin-textdomain' ),
    'parent_item_colon'  => __( 'Parent banner:', 'your-plugin-textdomain' ),
    'not_found'          => __( 'No banner found.', 'your-plugin-textdomain' ),
    'not_found_in_trash' => __( 'No banner found in Trash.', 'your-plugin-textdomain' )
  );

  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'your-plugin-textdomain' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'banner' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'         => array( 'title', 'page-attributes')
  );

  register_post_type( 'banner', $args );
}