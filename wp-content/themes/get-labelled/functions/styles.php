<?php 
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

remove_action( 'wp_head','feed_links', 2 );
remove_action( 'wp_head','feed_links_extra', 3 );

remove_action( 'wp_head', 'wp_resource_hints', 2 );

remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );

remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );

remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

remove_action( 'wp_head', 'wp_shortlink_wp_head');

remove_action ('wp_head', 'rsd_link');

remove_action( 'wp_head', 'wlwmanifest_link');

remove_action('wp_head', 'wp_generator');

remove_action('wp_head', 'index_rel_link'); 
remove_action('wp_head', 'wlwmanifest_link');

remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );

add_action( 'wp_enqueue_scripts', 'dequeue_junk', 999999 );
add_action( 'wp_head', 'dequeue_junk', 999999 );

function dequeue_junk() {
   	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style('yith-wcwl-font-awesome');
	wp_deregister_style('yith-wcwl-font-awesome');
	wp_dequeue_script( 'jquery-blockui' );
	wp_deregister_script( 'wp-embed' );

	wp_dequeue_style( 'wc-block-style' );
	wp_deregister_style( 'wc-block-style');
}

function so_enqueue_style() {
	// all actions related to emojis
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

	add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );

    wp_enqueue_style( 'main_preload', get_template_directory_uri().'/style.css','1', false );
    wp_enqueue_style( 'defered_preload', get_template_directory_uri().'/defered.css','1', false );

    if ( class_exists( 'WooCommerce' ) ) {
        wp_enqueue_style( 'woo_preload', get_template_directory_uri().'/woo.css','1', false );
    }
}
add_action('wp_enqueue_scripts', 'so_enqueue_style');

add_action('wp_head', function () {

    global $wp_styles;

  	// print_r($wp_styles);

    foreach($wp_styles->queue as $handle) {
        $style = $wp_styles->registered[$handle];
        // if(strpos($style->handle,'_preload') == true) {
        	$source = $style->src . ($style->ver ? "?ver={$style->ver}" : "?ver=".get_bloginfo('version'));
            echo "<link rel='preload' href='{$source}' as='style'/>\n";
        // }
    }
}, 1);

function ajs_enqueue_script() {
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script('plugin',	get_template_directory_uri().'/js/plugins.js','jquery','1',true);
	wp_enqueue_script('main',	get_template_directory_uri().'/js/main.js','jquery','1',true);
}

add_action('wp_enqueue_scripts', 'ajs_enqueue_script');


add_action('wp_head', function () {
    global $wp_scripts;
    foreach($wp_scripts->queue as $handle) {
        $script = $wp_scripts->registered[$handle];
        //-- Weird way to check if script is being enqueued in the footer.
        if($script->extra['group'] === 1) {
            //-- If version is set, append to end of source.
            $source = $script->src . ($script->ver ? "?ver={$script->ver}" : "");
            //-- Spit out the tag.
            echo "<link rel='preload' href='{$source}' as='script'/>\n";
        }
    }
}, 1);
