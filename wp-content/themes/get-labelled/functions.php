<?php
add_action( 'after_setup_theme', 'ajsSite_setup' );
function ajsSite_setup() {
	load_theme_textdomain( 'ajsSite', get_template_directory() . '/languages' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'html5', array( 'search-form' ) );
	global $content_width;
	if ( ! isset( $content_width ) ) { $content_width = 1920; }
	
	register_nav_menus( array( 
		'main-menu'	=> esc_html__( 'Main Menu', 'ajsSite' ),
		'footer-1' 	=> esc_html__( 'Footer Menu 1', 'ajsSite' ),
		'footer-2' 	=> esc_html__( 'Footer Menu 2', 'ajsSite' ),
		// 'footer-3' 	=> esc_html__( 'Footer Menu 3', 'ajsSite' ),
		'mobile-menu'=> esc_html__( 'Mobile Nav', 'ajsSite' )
	));
}

function mytheme_add_woocommerce_support() {

    add_theme_support( 'woocommerce', array(
        'product_grid'          => array(
            'default_rows'    => 3,
            'min_rows'        => 2,
            'max_rows'        => 8,
            'default_columns' => 4,
            'min_columns'     => 2,
            'max_columns'     => 5,
        ),
    ) );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

add_filter('use_block_editor_for_post', '__return_false', 10);

add_action( 'wp_footer', 'ajsSite_footer_scripts' );
function ajsSite_footer_scripts() { ?>
	<script>
	jQuery(document).ready(function ($) {
		var deviceAgent = navigator.userAgent.toLowerCase();
		if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
		$("html").addClass("ios");
		$("html").addClass("mobile");
		}
		if (navigator.userAgent.search("MSIE") >= 0) {
		$("html").addClass("ie");
		}
		else if (navigator.userAgent.search("Chrome") >= 0) {
		$("html").addClass("chrome");
		}
		else if (navigator.userAgent.search("Firefox") >= 0) {
		$("html").addClass("firefox");
		}
		else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
		$("html").addClass("safari");
		}
		else if (navigator.userAgent.search("Opera") >= 0) {
		$("html").addClass("opera");
		}
	});
	</script>
<?php }

add_action( 'widgets_init', 'ajsSite_widgets_init' );
function ajsSite_widgets_init() {
	register_sidebar( array(
	'name' => esc_html__( 'Sidebar Widget Area', 'ajsSite' ),
	'id' => 'primary-widget-area',
	'before_widget' => '<div id="%1$s" class="widget widget-container %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
}

if( function_exists('acf_add_options_page') ) {
	$parent = acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title' 	=> 'Theme Settings',
		'redirect' 		=> false
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Contact Details',
		'menu_title' 	=> 'Contact Details',
		'parent_slug' => $parent['menu_slug'],
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Banners',
		'menu_title' 	=> 'Banners',
		'parent_slug' => $parent['menu_slug'],
	));

	$options = get_fields('options');
}

//Remove Custom Fields from menu for other users, but not somarketing
//Prevents the adding of fields which then need to be added to the template
function remove_acf_menu(){
	global $current_user;
	if ($current_user->user_login!='somarketing'){
	remove_menu_page( 'edit.php?post_type=acf-field-group' );
	}
}
add_action( 'admin_menu', 'remove_acf_menu', 100 );


function get_the_logo() {
	global $options;
	$logo = get_template_directory_uri().'/img/logo.svg';
	if($options['logo']['id']) $logo = $options['logo']['url'];
	$html = '';

    if(is_front_page()) { 
    	$start 	= '<h1 id="branding">';
    	$end 	= '</h1>';
    } else {
    	$start 	= '<div id="branding">';
    	$end 	= '</div>';
    }
    $html .= $start;
	$html .= '<a href="/" title="'.get_bloginfo('name').'">';
		$html .= '<strong class="sr-only">'.get_bloginfo('name').'</strong>';
		$html .= '<img src="'.$logo.'" alt="'.get_bloginfo('name').' Logo" width="329px" height="54px" />';
	$html .= '</a>';
	$html .= $end;

	return $html;
}

function ajsSocial() {
	global $options;
	// print_r($options['social']);
	
	$html = '';
	$html .='<ul class="social">';
	      
	      if($options['social']['facebook']) { 
	        $html .='<li><a aria-label="Follow us on Facebook" href="https://www.facebook.com/'.$options['social']['facebook'].'" target="_blank" rel="noreferrer" title="Follow '.get_bloginfo('name').' on Facebook" class="fab fa-facebook-f"><span class="sr-only">Facebook</span></a></li>';
	      }
	      if($options['social']['twitter']) { 
	        $html .='<li><a aria-label="Follow us on Twitter" href="https://twitter.com/'.$options['social']['twitter'].'" target="_blank" rel="noreferrer" title="Follow '.get_bloginfo('name').' on Twitter" class="fab fa-twitter"><span class="sr-only">Twitter</span></a></li>';
	      }
	      if($options['social']['youtube']) { 
	        $html .='<li><a aria-label="Follow us on Youtube" href="https://www.youtube.com/channel/'.$options['social']['youtube'].'" target="_blank" rel="noreferrer" title="Follow '.get_bloginfo('name').' on Youtube" class="fab fa-youtube"><span class="sr-only">Youtube</span></a></li>';
	      }
	      if($options['social']['pinterest']) { 
	        $html .='<li><a aria-label="Follow us on Pinterest" href="https://www.pinterest.co.uk/'.$options['social']['pinterest'].'" target="_blank" rel="noreferrer" title="Follow '.get_bloginfo('name').' on Pinterest" class="fab fa-pinterest"><span class="sr-only">Pinterest</span></a></li>';
	      }
	      if($options['social']['instagram']) { 
	        $html .='<li><a aria-label="Follow us on Instagram" href="https://www.instagram.com/'.$options['social']['instagram'].'" target="_blank" rel="noreferrer" title="Follow '.get_bloginfo('name').' on Instagram" class="fab fa-instagram"><span class="sr-only">Instagram</span></a></li>';
	      }

	$html .='</ul>';
	return $html;
}

function wptuts_heartbeat_settings( $settings ) {
    $settings['interval'] = 10; //Anything between 15-60
    return $settings;
}
add_filter( 'heartbeat_settings', 'wptuts_heartbeat_settings' );

include_once('functions/cleanup-admin.php');

include_once('functions/styles.php');
include_once('functions/images.php');

include_once('functions/main-menu.php');

include_once('functions/cms.php');
include_once('functions/banner.php');
//woocommerce
if ( class_exists( 'WooCommerce' ) ) {
	include_once('functions/woo-extend.php');
}

//Ajax
include_once('ajax/so_footer.php');
include_once('ajax/load_controls.php');
include_once('ajax/enquiry-form.php');




?>
