  <?php global $options;
    $logo = $options['logo']['id'];
    if($options['logo_alt']) $logo = $options['logo_alt']['id'];
   ?>
   </main>
    <footer id="footer">
      <div>
        <a id="footer-branding" href="/" title="<?=get_bloginfo('name');?>">
          <?=ajsImage($logo); ?>
        </a>
        <?=clean_custom_menus('footer-1',2); ?>
        <?=clean_custom_menus('footer-2'); ?>
        <?php //clean_custom_menus('footer-3'); ?>
        <div class="contact">
          <strong class="title">Contact</strong>
          <address>
            <?=$options['address']; ?>
          </address>
          <p><a href="tel:<?=phoneURL($options['phone']); ?>" title="<?=$options['phone']; ?>"><?=$options['phone']; ?></a></p>
          <p><a href="mailto:<?=$options['email']; ?>"><?=$options['email']; ?></a></p>
        </div>
        <?=ajsSocial(); ?>
      </div>
    </footer>
    <div id="after-footer">
      <div>
        <p>&copy; <?=get_bloginfo('name');?> <?=date('Y');?>. All rights reserved</p>
        <?php if($options['cards']) { ?>
          <p class="cards"><span>Secure online payments:</span> <?=ajsImage($options['cards']['id']); ?></p>
        <?php } ?>
        <p class="so-stuff"></p>
      </div>
    </div>
  
  <?php include(get_template_directory().'/template-parts/mobile-menu.php'); ?>
  <script type="text/javascript">
    var THEME_URI = '<?= get_bloginfo("template_url"); ?>';
    var adminurl = '<?php echo admin_url ('admin-ajax.php'); ?>';
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&#038;libraries=places&#038;key=<?=$options['api_google_maps']; ?>"></script>
  <?php wp_footer(); ?>
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', '<?=$options['api_google_analytics']; ?>', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
  <script src="https://kit.fontawesome.com/571216e822.js" crossorigin="anonymous"></script>
  <?=$options['api_html']; ?>
</body>
</html>