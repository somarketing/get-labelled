<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}
global $product;
$fields = get_fields();
$background = $fields['background']['id'];
$post_thumbnail_id = get_post_thumbnail_id();
$wrapper_classes   = apply_filters(
	'woocommerce_single_product_image_gallery_classes',
	array(
		'woocommerce-product-gallery',
		'woocommerce-product-gallery--' . ( $post_thumbnail_id ? 'with-images' : 'without-images' ),
		'woocommerce-product-gallery--columns-' . absint( $columns ),
		'images',
	)
);
?>
<div id="product-gallery">
	<?php if($post_thumbnail_id) { ?>
	<div>
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<div class="main-image swiper-slide">
					<?php if($background) { ?>
						<picture class="background">
							<?=ajsImage($background,'product-background'); ?>
						</picture>
					<?php } ?>
					<picture class="image">
						<?php if (!$product->is_type( 'variable' ) ) { ?>
							<?=ajsImage($post_thumbnail_id,$size); ?>
						<?php } else { ?>
							<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
								<span class="woocommerce-product-gallery__wrapper">
									<?php
									if ( $post_thumbnail_id ) {
										$html = wc_get_gallery_image_html( $post_thumbnail_id, true );
									} else {
										$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
										$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src( 'woocommerce_single' ) ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
										$html .= '</div>';
									}
									echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id ); // phpcs:disable WordPress.XSS.EscapeOutput.OutputNotEscaped
									?>
								</span>
							</div>
						<?php } ?>
					</picture>
				</div>
				<?php
				$attachment_ids = $product->get_gallery_image_ids();

				if($attachment_ids && $product->get_image_id() ) {
					foreach ( $attachment_ids as $attachment_id ) { ?>
						<div class="thumbnail swiper-slide">
							<?=ajsImage($attachment_id,'product-thumb'); ?>
						</div>
					<?php
					}
				}
				?>
			</div>
		</div>
		<button class="product-prev"><span class="fa fa-angle-left"></span></button>
		<button class="product-next"><span class="fa fa-angle-right"></span></button>
	</div>
	<?php } ?>
</div>
