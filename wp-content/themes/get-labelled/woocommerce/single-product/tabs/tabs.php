<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */

global $product;
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>
	<div id="product-accordion" class="accordion">
		<?php
			$i = 0;
			foreach($tabs as $key => $tab) : 
				if($i != 0) {
					$aria = 'aria-expanded="false" ';
					$class = ' ';
				} else {
					$aria = 'aria-expanded="true" ';
					$class = ' open';
				}
				$tabData = '';
					$tabData = '<button ';
						$tabData .= 'id="tab-title-'.esc_attr( $key ).'" ';
					 	$tabData .= 'data-target="'.esc_attr( $key ).'" ';
					 	$tabData .= 'role="tab" ';
					 	$tabData .= $aria;
					 	$tabData .= 'class="'.$class.'" ';
					 	$tabData .= 'aria-label="View '.wp_kses_post( apply_filters( 'woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key ) ).'" ';
					 	$tabData .= 'aria-controls="'.esc_attr( $key ).'">';
							$tabData .= apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key );
					$tabData .= '</button>';
				$tabData .= '<section ';
					$tabData .= 'id="'.esc_attr( $key ).'"';
					$tabData .= 'role="tabpanel" ';
					$tabData .= $aria;
					$tabData .= 'aria-labelledby="tab-title-'.esc_attr( $key ).'" ';
					$tabData .= 'class="wysiwyg '.$class.'">';
					$tabData .= '<h2>'.apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ).'</h2>';
				$tabDataEnd = '</section>';
				if( call_user_func( $tab['callback'], $key, $tab ) !== null) {
					echo $tabData;
						if ( isset( $tab['callback'] ) ) { echo call_user_func( $tab['callback'], $key, $tab ); }
					echo $tabDataEnd;
				}
				$i++;
				//echo 'woops: '.$product->get_type().' and '.$key.'<br>';

			endforeach;
			echo '<button id="tab-title-reviews" data-target="reviews" role="tab" aria-expanded="false" class="" aria-label="View " aria-controls="reviews">Reviews</button>';
			echo '<section id="reviews" role="tabpanel" aria-expanded="false" aria-labelledby="tab-title-reviews" class="wysiwyg" style="">';
				comments_template();
			echo '</section>';
			

			do_action( 'woocommerce_product_after_tabs' );
		?>
	</div>
<?php endif; ?>
