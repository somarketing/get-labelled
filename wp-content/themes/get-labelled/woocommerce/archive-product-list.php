<?php

/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

$term = get_queried_object();
$id = $term->term_id;
$fields = get_fields('cpt_product');
if(get_fields('product_cat_'.$id)) {
  $fields = get_fields('product_cat_'.$id);
  $image = $fields['image'];
}
?>
<div id="product-archive">
  <button id="sidebar-toggle"><span class="fa fa-ballot-check"></span> <strong>Filter Products</strong></button>
  <div>
    <section >
      <?php
        if ( woocommerce_product_loop() ) {
          woocommerce_product_loop_start();
          if ( wc_get_loop_prop( 'total' ) ) {
            while ( have_posts() ) {
              the_post();
              do_action( 'woocommerce_shop_loop' );
              echo '<li class="product-wrap">';
                wc_get_template_part( 'content', 'product' );
              echo '</li>';
            }
          }
          woocommerce_product_loop_end();
        } else {
          echo "<h3>No products found.</h3>";
        }
      ?>
      <?php woocommerce_pagination(); ?>
    </section>
    <?php wc_get_template_part('shop-sidebar'); ?>
  </div>
</div>