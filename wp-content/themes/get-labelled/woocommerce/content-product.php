<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$productFields = get_fields();

$header = '<h3>'.$productFields['display_title']['line_1'].'</h3>';
if($productFields['display_title']['line_2']) {
	$header = '<h3>'.$productFields['display_title']['line_1'].'</h3>';
	$header .= '<p class="style">'.$productFields['display_title']['line_2'].'</p>';
}
$size = 'product-box';
?>
<a class="product-box" href="<?=get_the_permalink(); ?>" title="<?=wp_kses_post( $product->get_name() ); ?>">
	<div class="text">
		<?=$header; ?>
		<p class="price">
			<?=$product->get_price_html(); ?>
		</p>
	</div>
	<picture class="image">
		<?=ajsImage($productFields['thumbnail']['id'],$size); ?>
	</picture>
	<?php woocommerce_show_product_loop_sale_flash();?>
</a>