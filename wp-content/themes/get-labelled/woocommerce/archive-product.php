<?php

/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

do_action( 'woocommerce_before_main_content' );
$id = get_queried_object_id();
$term = get_term( $id, 'product_cat' );
$fields = get_fields('cpt_product');
$image = $fields['background_image'];
if(get_fields('product_cat_'.$id)) {
    $fields = get_fields('product_cat_'.$id);
    if($fields['background_image']) {
      $image = $fields['background_image'];
    }
}

  $display_type = woocommerce_get_loop_display_mode();
  if(is_shop()) {
    $display_type = 'both';
  }
    
  // print_r($display_type);
  switch ($display_type) {
    default:
      include(locate_template('/template-parts/page-title.php'));
      wc_get_template_part('archive-product-list');
      break;
  }


/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
// do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
