<?php 
$fields = get_fields('cpt_product');
$banners[0] = $fields['sidebar-banners'];

$args = array(
  'post_type'       => 'banner',
  'posts_per_page'  => 1,
  'post__in'      => $banners,
);
$tposts = new WP_Query( $args );
?>
<aside id="sidebar">
  <div>
    <?php dynamic_sidebar( 'primary-widget-area' );?>

    <?php 
    if($tposts->have_posts()):
    while($tposts->have_posts()): 
      $tposts->the_post();
      global $post_object;
      $bannerFields = get_fields();
      $background = '';
      if($bannerFields['image']['id']) {
        $background = ajsImage($bannerFields['image']['id'],'banner');
      }
      $class = ' banner-'.get_the_ID().' ';
      if($bannerFields['content']['text_color']) $class .= $bannerFields['content']['text_color'].' ';
      if($bannerFields['content']['logo']) $class .= ' has-logo';
      include(locate_template('/template-parts/banner-box.php'));
      endwhile;
    endif;
    wp_reset_postdata();
    ?>
  </div>
</aside>