<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$fields = get_fields();
$product_attributes = $product->get_attributes();
$featured_attrs = $fields['featured_attrs'];
$featured_attrs = explode(PHP_EOL,$featured_attrs);
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$preTitle = '';
$title = '<strong>'.get_the_title().'</strong>';

if($product->get_attribute( 'pa_brand' )) {
	$preTitle = '<span class="brand">'.$product->get_attribute( 'pa_brand' ).'</span>';
}

if($fields['display_title']['line_1']) {
	$title = '<strong>'.$fields['display_title']['line_1'].'</strong>';
}
if($fields['display_title']['line_2']) {
	$title .= '<span class="style">'.$fields['display_title']['line_2'].'</span>';
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
<div id="page-header">
	<div>
		<?php woocommerce_breadcrumb(); ?>
	</div>
</div>
<article id="product-single">
	<div>
		<header id="product-title">
			<?php
				ajs_stockstatus();
				echo ajs_attribute(true); 
				if($fields['icon']) echo '<div class="compass">'.ajsImage($fields['icon']['id']).'</div>';
			?>
		</header>
		<?php
		/**
		* Hook: woocommerce_before_single_product_summary.
		*
		* @hooked woocommerce_show_product_sale_flash - 10
		* @hooked woocommerce_show_product_images - 20
		*/
		do_action('woocommerce_before_single_product_summary');
		?>
		<section id="product-description">
			<?php
				echo '<p class="short-description">'.$fields['short_description'].'</p>';
				echo '<h1>'.$preTitle.$title.'</h1>';
				if($product->get_sku()) echo '<p class="sku">SKU: '.$product->get_sku().'</p>';
				woocommerce_template_single_price();
				if($fields['show_button'] !== false) woocommerce_template_single_add_to_cart();
				woocommerce_output_product_data_tabs();
			?>
		</section>

	</div>
</article>
</div>
<?php woocommerce_upsell_display();
do_action( 'woocommerce_after_single_product' ); ?>