<?php 
	$banners = get_sub_field('banner');
	$posts = 'banner';
	$bg = get_sub_field('background');
	$count = count($banners);

	$args = array(
		'post_type'      	=> $posts,
		'posts_per_page'	=> 2,
		'post__in'			=> $banners,
	);
	$tposts = new WP_Query( $args );
?>
<aside class="banners count-<?=$count; ?>">
	<div>
		<?php 
		if($tposts->have_posts()):
		while($tposts->have_posts()): 
			$tposts->the_post();
			global $post_object;
			$bannerFields = get_fields();
			$background = '';
			if($bannerFields['image']['id']) {
				$background = ajsImage($bannerFields['image']['id'],'banner');
			}
			$class = ' banner-'.get_the_ID().' ';
			if($bannerFields['content']['text_color']) $class .= $bannerFields['content']['text_color'].' ';
			if($bannerFields['content']['logo']) $class .= ' has-logo';
			include(locate_template('/template-parts/banner-box.php'));
			endwhile;
		endif;
		wp_reset_postdata();
		?>
	</div>
</aside>