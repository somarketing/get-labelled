<?php 
	$title = get_sub_field('title');
	$grid = get_sub_field('grid');
	$count = count($grid);
	$class = '';
	$class .= ' count-'.$count;
	$bg = get_sub_field('background');
?>
<section class="featured-grid <?=$bg; ?>">
	<div>
		<h2><?=$title; ?></h2>
		<ul class="<?=$class; ?>">
			<?php foreach ($grid as $box) {

			?>
			<li class="grid-box">
				<a href="<?=$box['content']['link']['url']; ?>" title="<?=$box['content']['title']; ?>" >
					<div class="background">
						<?=ajsImage($box['background']['id'],'banner'); ?>
					</div>
					<div class="text">
						<h3><?=$box['content']['title']; ?></h3>
						<p><?=$box['content']['snippet']; ?></p>
						<?php if($box['content']['link']['title']) { ?>
							<span class="button white"><?=$box['content']['link']['title']; ?></span>
						<?php } ?>
					</div>
				</a>
			</li>
			<?php } ?>
		</ul>
	</div>
</section>