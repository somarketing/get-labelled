<?php 
	$title = get_sub_field('title');
	$brands = get_sub_field('brands');
	$bg = get_sub_field('background');
	// print_r($brands);
?>
<aside class="brands <?=$bg; ?>">
	<div>
		<h2><?=$title; ?></h2>
		<div class="swiper-outer">
			<div class="swiper-container">
				<ul class="swiper-wrapper">
					<?php foreach($brands as $brand) {
						// print_r($brand);
						$mediaID = $brand['ID'];
						$url = get_field('url',$mediaID);
			            $html = '<li class="swiper-slide">';
							if(isset($url)) { 
								$html .= '<a class="brand" href="'.$url.'" title="'.$brand['alt'].'">';
							} else {
								$html .= '<span class="brand">';
							}
							$html .= ajsImage($brand['id'],'brand');
							if(isset($url)) {
								$html .= '</a>';
							} else {
								$html .= '</span>';
							}
						$html .= '</li>';
						echo $html;
					} ?>
					</li>
				</ul>
			</div>
		</div>
	</div>
</aside>