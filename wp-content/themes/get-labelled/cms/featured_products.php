<?php 
if ( class_exists( 'WooCommerce' ) ) {
$title = get_sub_field('title');
$posts = array('product');
$products = get_sub_field('products');
$bg = get_sub_field('background');

$args = array(
	'post_type'      	=> $posts,
	'posts_per_page'	=> 50,
	'post__in'			=> $products,
	'orderby'           => 'post__in',
);
$tposts = new WP_Query( $args );
?>
<section class="featured-products <?=$bg; ?>">
	<div>
		<h2><?=$title; ?></h2>
		<div class="swiper-outer">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<?php 
					if($tposts->have_posts()):
					while($tposts->have_posts()): 
						$tposts->the_post();
						global $post_object;
						?>
							<div class="swiper-slide product-wrap">
								<?php wc_get_template_part('content','product'); ?>
							</div>
							<?php
						endwhile;
					endif;
					wp_reset_postdata();
					?>
				</div>
	    	</div>
	    	<nav>
				<button class="swiper-nav swiper-prev"></button>
				<button class="swiper-nav swiper-next"></button>
			</nav>
		</div>
	</div>
</section>
<?php } else { ?>
	<section class="featured-products <?=$bg; ?>">
		Sorry woocommerce is not installed.
	</section>
<?php } ?>