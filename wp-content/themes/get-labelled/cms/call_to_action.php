<?php
	$content = get_sub_field('content');
	$image = get_sub_field('image');
	$bg = get_sub_field('background');	
?>
<div class="call-to-action <?=$bg; ?>">
	<div>
		<a href="<?=$content['link']['url']; ?>" title="<?=$content['link']['title']; ?>">
			<?=ajsImage($image['id'],'full'); ?>
			<div class="text">
				<strong class="title"><?=$content['title']; ?></strong>
				<p><?=$content['snippet']; ?></p>
				<?php if($content['link']['title']) { ?>
					<span class="button white"><?=$content['link']['title']; ?></span>
				<?php } ?>
			</div>
		</a>
	</div>
</div>