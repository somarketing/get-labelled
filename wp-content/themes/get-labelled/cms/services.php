<?php 
	$services = get_sub_field('services');
	$bg = get_sub_field('background');
?>
<section class="services <?=$bg; ?>">
	<div>
		<h2>Services</h2>
		<ul>
			<?php foreach ($services as $service) { ?>
			<li>
				<a href="<?=$service['content']['url']; ?>" title="<?=$service['link']['title']; ?>">
					<h3><?=$service['content']['title']; ?></h3>
					<p><?=$service['content']['text']; ?></p>
					<div class="img">
						<?=ajsImage($service['image']['id']); ?>
					</div>
				</a>
			</li>
			<?php } ?>
		</ul>
	</div>
</section>