<?php 
	$video = get_sub_field('video');
	$poster = get_sub_field('poster');
?>
<figure class="featured-video video-cms">
	<video poster="<?=$poster['url']; ?>">
		<source src="<?=$video; ?>">
	</video>
	<button aria-label="Play Featured Video"></button>
</figure>