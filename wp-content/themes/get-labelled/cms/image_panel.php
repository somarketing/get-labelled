<?php
	$content = get_sub_field('content');
	$image = get_sub_field('image');
	$class = get_sub_field('background').' ';
	if($content !== '') $class .= 'text ';
?>
<div class="image-panel <?=$class; ?>">
	<figure>
		<?=ajsImage($image['id'],'full'); ?>
		<?php if($content !== '') { ?>
			<figcaption>
			<?=$content['wysiwyg']; ?>
		</figcaption>
	<?php } ?>
	</figure>
</div>