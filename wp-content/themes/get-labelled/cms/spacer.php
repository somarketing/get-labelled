<?php 
	$height = get_sub_field('height');
	$bg = get_sub_field('background');
?>
<div class="spacer <?=$bg; ?>" style="min-height: <?=$height; ?>px;"></div>