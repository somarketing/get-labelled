<?php 
	$wysiwyg = get_sub_field('wysiwyg');
	$image = get_sub_field('image');
	$align = get_sub_field('images_pos');
	$video = get_sub_field('video_link');
	$bg = get_sub_field('background');
?>
<div class="wysiwyg-image <?=$align; ?> <?=$bg; ?>">
	<div>
		<section class="wysiwyg">
			<?=$wysiwyg; ?>
		</section>
		<figure>
			<?php if($video) { ?>
			<div class="video-cms">
				<video poster="<?=$image['url']; ?>">
					<source src="<?=$video; ?>">
				</video>
				<button aria-label="Play Video"></button>
			</div>
			<?php } else { ?>
				<img src="<?=$image['url']; ?>" alt="<?=$image['alt']; ?>" />
			<?php } ?>
		</figure>
	</div>
</div>