<?php 
	$wysiwyg = get_sub_field('wysiwyg');
	$bg = get_sub_field('background');
?>
<div class="wysiwyg-full <?=$bg; ?>">
	<section class="wysiwyg">
		<?=$wysiwyg; ?>
	</section>
</div>
