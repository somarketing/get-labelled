<?php 
	$height = get_sub_field('height');
	$bg = get_sub_field('background');
?>
<div class="break" style="margin: <?=$height/2; ?>px auto;">
	<div class="<?=$bg; ?>"></div>
</div>